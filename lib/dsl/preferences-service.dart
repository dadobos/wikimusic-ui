import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/model/app.dart';

class UserPreferencesChanged extends Notification {
  UserPreferencesChanged({required this.userPreferences});
  final UserPreferences userPreferences;
}

class UserPreferences {
  final SortOrder artistSorting;
  final SortOrder genreSorting;
  final SortOrder songSorting;

  UserPreferences(
      {required this.artistSorting,
      required this.genreSorting,
      required this.songSorting});

  UserPreferences withSongSorting(final SortOrder sortOrder) {
    return UserPreferences(
        artistSorting: artistSorting,
        genreSorting: genreSorting,
        songSorting: sortOrder);
  }

  UserPreferences withGenreSorting(final SortOrder sortOrder) {
    return UserPreferences(
        artistSorting: artistSorting,
        genreSorting: sortOrder,
        songSorting: songSorting);
  }

  UserPreferences withArtistSorting(final SortOrder sortOrder) {
    return UserPreferences(
        artistSorting: sortOrder,
        genreSorting: genreSorting,
        songSorting: songSorting);
  }

  static Future<UserPreferences> fromStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return UserPreferences(
        artistSorting: SortOrder.values
            .byName(prefs.getString("artistSorting") ?? "createdAtDesc"),
        genreSorting: SortOrder.values
            .byName(prefs.getString("genreSorting") ?? "createdAtDesc"),
        songSorting: SortOrder.values
            .byName(prefs.getString("songSorting") ?? "createdAtDesc"));
  }

  Future<void> toStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("artistSorting", artistSorting.name);
    prefs.setString("genreSorting", genreSorting.name);
    prefs.setString("songSorting", songSorting.name);
  }
}

class UserPreferencesProvider extends InheritedWidget {
  final ValueNotifier<UserPreferences> userPreferences;

  const UserPreferencesProvider(
      {super.key, required super.child, required this.userPreferences});

  static UserPreferencesProvider? maybeOf(final BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<UserPreferencesProvider>();
  }

  static UserPreferencesProvider of(final BuildContext context) {
    final UserPreferencesProvider? result = maybeOf(context);
    assert(result != null, 'No L10NProvider found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(covariant final UserPreferencesProvider oldWidget) {
    return oldWidget.userPreferences != userPreferences;
  }

  static Future<void> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("authToken");
    prefs.remove("roles");
    prefs.remove("lastEmailUsed");
    prefs.remove("identifier");
    prefs.remove("displayName");
  }

  static Future<bool> hasPreviousSession(final Env env) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final authToken = prefs.getString('authToken');
    return authToken != null && authToken.isNotEmpty;
  }

  static Future<void> setString(final String key, final String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String?> getString(final String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final s = prefs.getString(key);
    return s;
  }
}
