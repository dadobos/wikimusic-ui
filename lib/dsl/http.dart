import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:logging/logging.dart';
import 'package:wikimusic_ui/dsl/env.dart';

Future<Response<dynamic>> doHttp(final Env env, final String uri,
    {final Map<String, dynamic>? data,
    final String? authToken,
    final int secondsTimeout = 8,
    final String method = "GET"}) async {
  final logger = Logger("doHttp");
  logger.log(Level.FINEST, uri);

  final headers = {'content-type': 'application/json; charset=UTF-8'};
  if (authToken != null) {
    headers["x-wikimusic-auth"] = authToken;
  }
  if (method == "GET") {
    headers['accept'] = 'application/json';
  }
  final r = await env.dio.request<dynamic>(uri,
      data: data != null ? jsonEncode(data) : null,
      options: Options(
          headers: headers,
          receiveTimeout: Duration(seconds: secondsTimeout),
          method: method));

  logger.log(Level.FINEST, r.statusCode);
  logger.log(Level.FINEST, r.headers);
  return r;
}

String? extractJWT(final Response<dynamic> r) {
  return r.headers["x-wikimusic-auth"]?.first;
}
