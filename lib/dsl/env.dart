import 'package:dio/dio.dart';
import 'package:time_machine/time_machine.dart';

class Env {
  Env(
      {required this.dio,
      required this.apiUrl,
      this.authToken,
      this.lastEmailUsed,
      this.prefersDark,
      this.locale,
      this.lastLoggedInAt});

  final Dio dio;
  final String apiUrl;
  String? authToken;
  String? lastEmailUsed;
  ZonedDateTime? lastLoggedInAt;
  bool? prefersDark;
  String? locale;
}
