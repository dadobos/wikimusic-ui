import 'package:flutter/material.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';

class ValidatedForm extends StatefulWidget {
  const ValidatedForm(
      {super.key,
      required this.env,
      required this.widget,
      required this.onSubmit,
      required this.controllers,
      this.submitText});

  final Env env;
  final Widget widget;
  final VoidCallback onSubmit;
  final String? submitText;
  final Map<String, TextEditingController> controllers;

  @override
  ValidatedFormState createState() => ValidatedFormState();
}

class ValidatedFormState extends State<ValidatedForm> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(final BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.widget,
              verticalSpacer(height: 30),
              submitButton(() {
                if (formKey.currentState!.validate()) {
                  widget.onSubmit();
                  for (final controller in widget.controllers.values) {
                    controller.clear();
                  }
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text(t(context).forms.goodSubmission)),
                  );
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text(t(context).forms.badData)),
                  );
                }
              }, widget.submitText ?? t(context).buttons.submit),
            ]));
  }
}
