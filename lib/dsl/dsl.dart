import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wikimusic_ui/l10n/l10n.dart';

Widget horizontalSpacer({final double width = 20}) => Container(
      width: width,
      color: Colors.transparent,
    );
Widget verticalSpacer({final double height = 20}) => Container(
      height: height,
      color: Colors.transparent,
    );

Widget submitButton(final VoidCallback onSubmit, final String text) =>
    ElevatedButton(
      onPressed: onSubmit,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.check),
          Text(text,
              style: const TextStyle(
                fontWeight: FontWeight.w900,
                // color: Colors.green
              ))
        ],
      ),
    );

Widget primaryButton(final VoidCallback onSubmit, final String text) =>
    ElevatedButton(
      onPressed: onSubmit,
      child: Text(
        text,
        style: const TextStyle(
          fontWeight: FontWeight.w700,
          // color: Color.fromRGBO(209, 126, 2, 1),
        ),
      ),
    );

String? Function(String?) defaultValidator = (final String? value) {
  if (value == null || value.isEmpty || value.trim() == "") {
    return "Value cannot be empty!";
  }
  return null;
};

String? Function(String?) emailValidator = (final String? value) {
  if (value == null ||
      value.isEmpty ||
      value.trim() == "" ||
      !value.contains("@") ||
      !value.contains(".")) {
    return "Value must be a valid email address!";
  }
  return null;
};

Widget requiredField(final TextEditingController? c,
        {final String label = "",
        final String errorMessage = "",
        final FormFieldValidator<String>? validator,
        final TextInputType keyboardType = TextInputType.text,
        final int? minLines,
        final int? maxLines,
        final TextStyle? style}) =>
    TextFormField(
        decoration: InputDecoration(
          labelText: label,
        ),
        controller: c,
        validator: validator ?? defaultValidator,
        keyboardType: keyboardType,
        minLines: minLines,
        maxLines: maxLines,
        style: style);

Widget passwordField(final TextEditingController? c,
        {final String label = "",
        final String errorMessage = "",
        final FormFieldValidator<String>? validator,
        final TextInputType keyboardType = TextInputType.text,
        final int? minLines,
        final int? maxLines,
        final TextStyle? style}) =>
    TextFormField(
        decoration: InputDecoration(
          labelText: label,
        ),
        controller: c,
        obscureText: true,
        validator: validator ?? defaultValidator,
        keyboardType: keyboardType,
        style: style);

Widget optionalField(final TextEditingController? c,
        {final String label = "",
        final String errorMessage = "",
        final FormFieldValidator<String>? validator,
        final TextInputType keyboardType = TextInputType.text,
        final int? minLines,
        final int? maxLines,
        final TextStyle? style}) =>
    TextFormField(
      decoration: InputDecoration(
        labelText: label,
      ),
      controller: c,
      validator: validator,
      keyboardType: keyboardType,
      minLines: minLines,
      maxLines: maxLines,
      style: style,
    );

Widget padding(final Widget child, {final double insets = 30.0}) =>
    Padding(padding: EdgeInsets.all(insets), child: child);

Widget header1(final BuildContext context, final String text) => Text(
      text,
      style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: Theme.of(context).textTheme.headlineLarge?.fontSize),
    );

Widget header2(final BuildContext context, final String text) => Text(
      text,
      style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: Theme.of(context).textTheme.headlineMedium?.fontSize),
    );

Widget header3(final BuildContext context, final String text) => Text(
      text,
      style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
    );

Widget table(final List<DataColumn> columns, final List<DataRow> rows) =>
    DataTable(columns: columns, rows: rows);

Widget twoColTable(
        {final List<DataColumn> columns = const [
          DataColumn(label: Text("")),
          DataColumn(label: Text(""))
        ],
        final List<DataRow> rows = const []}) =>
    DataTable(columns: columns, rows: rows);

DataRow twoColRow(
        final BuildContext context, final String left, final String right) =>
    DataRow(cells: [
      DataCell(Text(left,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Theme.of(context).colorScheme.inverseSurface))),
      DataCell(Text(right))
    ]);
DataRow twoColRowWidget(
        final BuildContext context, final String left, final Widget right) =>
    DataRow(cells: [
      DataCell(Text(left,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Theme.of(context).colorScheme.inverseSurface))),
      DataCell(right)
    ]);

Widget wrap(
        {final List<Widget> children = const [],
        final double spacing = 12.0,
        final double runSpacing = 10.0}) =>
    Wrap(
      spacing: spacing,
      runSpacing: runSpacing,
      children: children,
    );

Widget monospacedText(final String text, final double? fontSize) =>
    SelectableText(text,
        style: GoogleFonts.robotoMono(
          textStyle: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.w500,
          ),
        ));

Widget link(final String uri, {final String label = "Click here"}) => InkWell(
      onTap: () {
        try {
          launchUrl(Uri.parse(uri));
          // ignore: empty_catches
        } on Exception {}
      },
      child: Text(
        label,
        style: const TextStyle(
          decoration: TextDecoration.underline,
          //  color: Colors.blue
        ),
      ),
    );

String? fromTextController(final TextEditingController? controller) {
  if (controller == null || controller.text == "") {
    return null;
  } else {
    return controller.text;
  }
}

Widget scrollableArea(final List<Widget> children,
        {final MainAxisAlignment mainAxisAlignment =
            MainAxisAlignment.center}) =>
    SingleChildScrollView(
        physics: const ScrollPhysics(),
        child:
            Column(mainAxisAlignment: mainAxisAlignment, children: children));

double deviceWidth(final BuildContext context, {final double scale = 1.0}) =>
    MediaQuery.of(context).size.width * scale;

double deviceHeight(final BuildContext context, {final double scale = 1.0}) =>
    MediaQuery.of(context).size.height * scale;

Widget deleteButton(final BuildContext context, final String title,
        final VoidCallback eff) =>
    ElevatedButton(
        onPressed: () async {
          final d = makeConfirmPrompt(context, title: title, () async {
            eff();
          });
          showDialog<Widget>(
              context: context, builder: (final BuildContext context) => d);
        },
        child: const Icon(
          Icons.delete,
          // color: Colors.orange,
        ));

Widget makeConfirmPrompt(final BuildContext context, final VoidCallback onDo,
        {final String title = "",
        final VoidCallback? onDont,
        final String? body}) =>
    AlertDialog(
      title: Text(title),
      content: body != null ? Text(body) : null,
      actions: [
        TextButton(
            onPressed: onDont ?? () => Navigator.pop(context),
            child: const Text("CANCEL")),
        TextButton(
            child: const Text("OK"),
            onPressed: () {
              onDo();
              Navigator.pop(context);
            })
      ],
    );

AppLocalization t(final BuildContext context) =>
    TranslationProvider.of(context).languageSettings.value.appLocalization;

class Debounce {
  Duration delay;
  Timer? timer;

  Debounce(this.delay);

  void call(final void Function() callback) {
    timer?.cancel();
    timer = Timer(delay, callback);
  }

  void dispose() {
    timer?.cancel();
  }
}

Future<bool> isAtLeastRole(final bool Function(String element) test) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final List<String> r = prefs.getStringList("roles") ?? [];
  return r.where(test).firstOrNull != null;
}

Future<bool> isAtLeastDemo() async => isAtLeastRole((final role) =>
    role == "wm::demo" ||
    role == "wm::lowrank" ||
    role == "wm::maintainer" ||
    role == "wm::superuser");

Future<bool> isAtLeastLowRank() async => isAtLeastRole((final role) =>
    role == "wm::lowrank" ||
    role == "wm::maintainer" ||
    role == "wm::superuser");

Future<bool> isAtLeastMaintainer() async => isAtLeastRole(
    (final role) => role == "wm::maintainer" || role == "wm::superuser");

Future<bool> isAtLeastSuperUser() async =>
    isAtLeastRole((final element) => element == "wm::superuser");

Widget drawForSuperUsers(final Widget w) =>
    maybeDrawFuture(isAtLeastSuperUser(), w);
Widget drawForMaintainers(final Widget w) =>
    maybeDrawFuture(isAtLeastMaintainer(), w);
Widget drawForLowRank(final Widget w) => maybeDrawFuture(isAtLeastLowRank(), w);
Widget drawForDemo(final Widget w) => maybeDrawFuture(isAtLeastDemo(), w);

Widget maybeDrawFuture(final Future<bool>? eff, final Widget w) =>
    FutureBuilder(
        future: eff,
        builder: (final context, final snapshot) {
          return Visibility(
              visible: snapshot.data != null && snapshot.data == true,
              child: w);
        });
