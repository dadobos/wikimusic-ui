import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/pages/about.dart';
import 'package:wikimusic_ui/pages/command/artist-add.dart';
import 'package:wikimusic_ui/pages/command/artist-edit.dart';
import 'package:wikimusic_ui/pages/command/genre-add.dart';
import 'package:wikimusic_ui/pages/command/genre-edit.dart';
import 'package:wikimusic_ui/pages/command/reset-password.dart';
import 'package:wikimusic_ui/pages/command/song-add.dart';
import 'package:wikimusic_ui/pages/command/song-content-add.dart';
import 'package:wikimusic_ui/pages/command/song-content-edit.dart';
import 'package:wikimusic_ui/pages/command/song-edit.dart';
import 'package:wikimusic_ui/pages/command/users-delete.dart';
import 'package:wikimusic_ui/pages/command/users-invite.dart';
import 'package:wikimusic_ui/pages/login.dart';
import 'package:wikimusic_ui/pages/management.dart';
import 'package:wikimusic_ui/pages/query/artist-detail.dart';
import 'package:wikimusic_ui/pages/query/artist-list.dart';
import 'package:wikimusic_ui/pages/query/genre-detail.dart';
import 'package:wikimusic_ui/pages/query/genre-list.dart';
import 'package:wikimusic_ui/pages/query/preferences.dart';
import 'package:wikimusic_ui/pages/query/song-detail.dart';
import 'package:wikimusic_ui/pages/query/song-list.dart';

GoRouter makeRouter(final Env env) => GoRouter(
      errorBuilder: (final context, final state) =>
          LoginPage(title: t(context).routeNames.login, env: env),
      routes: [
        GoRoute(
          path: '/',
          builder: (final context, final state) {
            context.go("/songs");
            return const Column(children: []);
          },
        ),
        GoRoute(
          path: '/login',
          builder: (final context, final state) =>
              LoginPage(title: t(context).routeNames.login, env: env),
        ),
        GoRoute(
          path: '/songs',
          builder: (final context, final state) =>
              SongListPage(title: t(context).routeNames.songs, env: env),
        ),
        GoRoute(
          path: '/songs/:identifier',
          builder: (final context, final state) => SongDetailPage(
            title: t(context).routeNames.songs,
            env: env,
            identifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/artists',
          builder: (final context, final state) => ArtistListPage(
            title: t(context).routeNames.artists,
            env: env,
          ),
        ),
        GoRoute(
          path: '/artists/:identifier',
          builder: (final context, final state) => ArtistDetailPage(
            title: t(context).routeNames.artists,
            env: env,
            identifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/genres',
          builder: (final context, final state) =>
              GenreListPage(title: t(context).routeNames.genres, env: env),
        ),
        GoRoute(
          path: '/genres/:identifier',
          builder: (final context, final state) => GenreDetailPage(
            title: t(context).routeNames.genres,
            env: env,
            identifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/preferences',
          builder: (final context, final state) => PreferencesPage(
              title: t(context).routeNames.preferences, env: env),
        ),
        GoRoute(
          path: '/management',
          builder: (final context, final state) =>
              ManagementPage(title: t(context).routeNames.management, env: env),
        ),
        GoRoute(
          path: '/about',
          builder: (final context, final state) =>
              AboutPage(title: "About WikiMusic", env: env),
        ),
        GoRoute(
          path: '/management/songs/add',
          builder: (final context, final state) => SongAddPage(env: env),
        ),
        GoRoute(
          path: '/management/genres/add',
          builder: (final context, final state) => GenreAddPage(env: env),
        ),
        GoRoute(
          path: '/management/artists/add',
          builder: (final context, final state) => ArtistAddPage(env: env),
        ),
        GoRoute(
          path: '/management/artists/edit/:identifier',
          builder: (final context, final state) => ArtistEditPage(
            env: env,
            artistIdentifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/management/genres/edit/:identifier',
          builder: (final context, final state) => GenreEditPage(
            env: env,
            genreIdentifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/management/songs/edit/:identifier',
          builder: (final context, final state) => SongEditPage(
            env: env,
            songIdentifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/management/songs/contents/add/:identifier',
          builder: (final context, final state) => SongContentAddPage(
            env: env,
            songIdentifier: state.pathParameters['identifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/management/songs/:songIdentifier/contents/edit/:identifier',
          builder: (final context, final state) => SongContentEditPage(
            env: env,
            contentIdentifier: state.pathParameters['identifier'] ?? "",
            songIdentifier: state.pathParameters['songIdentifier'] ?? "",
          ),
        ),
        GoRoute(
          path: '/management/users/add',
          builder: (final context, final state) => InviteUsersPage(env: env),
        ),
        GoRoute(
          path: '/management/users/delete',
          builder: (final context, final state) => DeleteUsersPage(env: env),
        ),
        GoRoute(
          path: '/user/reset-password/token',
          builder: (final context, final state) => ResetPasswordPage(
              env: env, token: state.uri.queryParameters["token"] ?? ""),
        ),
      ],
    );
