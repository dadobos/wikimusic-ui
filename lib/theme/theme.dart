import 'package:flutter/material.dart';
import 'package:material_color_utilities/material_color_utilities.dart';

class NoAnimationPageTransitionsBuilder extends PageTransitionsBuilder {
  const NoAnimationPageTransitionsBuilder();

  @override
  Widget buildTransitions<T>(
    final PageRoute<T> route,
    final BuildContext context,
    final Animation<double> animation,
    final Animation<double> secondaryAnimation,
    final Widget child,
  ) {
    return child;
  }
}

class ThemeSettingChange extends Notification {
  ThemeSettingChange({required this.settings});
  final ThemeSettings settings;
}

class ThemeProvider extends InheritedWidget {
  const ThemeProvider(
      {super.key,
      required this.settings,
      required this.lightDynamic,
      required this.darkDynamic,
      required super.child});

  final ValueNotifier<ThemeSettings> settings;
  final ColorScheme? lightDynamic;
  final ColorScheme? darkDynamic;

  final pageTransitionsTheme = const PageTransitionsTheme(
    builders: <TargetPlatform, PageTransitionsBuilder>{
      TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
      TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      TargetPlatform.linux: NoAnimationPageTransitionsBuilder(),
      TargetPlatform.macOS: NoAnimationPageTransitionsBuilder(),
      TargetPlatform.windows: NoAnimationPageTransitionsBuilder(),
    },
  );

  Color custom(final CustomColor custom) {
    if (custom.blend) {
      return blend(custom.color);
    } else {
      return custom.color;
    }
  }

  Color blend(final Color targetColor) {
    return Color(
        Blend.harmonize(targetColor.value, settings.value.sourceColor.value));
  }

  Color source(final Color? target) {
    Color source = settings.value.sourceColor;
    if (target != null) {
      source = blend(target);
    }
    return source;
  }

  ColorScheme colors(final Brightness brightness, final Color? targetColor) {
    final dynamicPrimary = brightness == Brightness.light
        ? lightDynamic?.primary
        : darkDynamic?.primary;
    return ColorScheme.fromSeed(
      seedColor: dynamicPrimary ?? source(targetColor),
      brightness: brightness,
    );
  }

  ShapeBorder get shapeMedium => RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      );

  CardTheme cardTheme() {
    return CardTheme(
      elevation: 0,
      shape: shapeMedium,
      clipBehavior: Clip.antiAlias,
    );
  }

  ListTileThemeData listTileTheme(final ColorScheme colors) {
    return ListTileThemeData(
      shape: shapeMedium,
      selectedColor: colors.secondary,
    );
  }

  AppBarTheme appBarTheme(final ColorScheme colors) {
    return AppBarTheme(
      elevation: 0,
      backgroundColor: colors.surfaceContainerHighest,
      foregroundColor: colors.onSurface,
    );
  }

  TabBarTheme tabBarTheme(final ColorScheme colors) {
    return TabBarTheme(
      labelColor: colors.secondary,
      unselectedLabelColor: colors.onSurfaceVariant,
      indicator: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: colors.secondary,
            width: 2,
          ),
        ),
      ),
    );
  }

  BottomAppBarTheme bottomAppBarTheme(final ColorScheme colors) {
    return BottomAppBarTheme(
      color: colors.surface,
      elevation: 0,
    );
  }

  BottomNavigationBarThemeData bottomNavigationBarTheme(
      final ColorScheme colors) {
    return BottomNavigationBarThemeData(
      type: BottomNavigationBarType.fixed,
      backgroundColor: colors.surfaceContainerHighest,
      selectedItemColor: colors.onSurface,
      unselectedItemColor: colors.onSurfaceVariant,
      elevation: 0,
      landscapeLayout: BottomNavigationBarLandscapeLayout.centered,
    );
  }

  NavigationRailThemeData navigationRailTheme(final ColorScheme colors) {
    return const NavigationRailThemeData();
  }

  DrawerThemeData drawerTheme(final ColorScheme colors) {
    return DrawerThemeData(
      backgroundColor: colors.surfaceContainerHighest,
    );
  }

  NavigationBarThemeData navigationBarTheme(final ColorScheme colors) =>
      NavigationBarThemeData(backgroundColor: colors.surfaceContainerHighest);

  DropdownMenuThemeData dropdownMenuTheme(final ColorScheme colors) =>
      DropdownMenuThemeData(
          inputDecorationTheme: InputDecorationTheme(
              fillColor: colors.surfaceContainer,
              filled: true,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16))));

  ButtonThemeData buttonTheme(final ColorScheme colors) =>
      const ButtonThemeData(
        textTheme: ButtonTextTheme.normal,
      );

  ThemeData light([final Color? targetColor]) {
    final colorScheme = colors(Brightness.light, targetColor);
    return ThemeData(
            brightness: Brightness.light,
            useMaterial3: true,
            fontFamily: "Aileron")
        .copyWith(
            pageTransitionsTheme: pageTransitionsTheme,
            colorScheme: colorScheme,
            appBarTheme: appBarTheme(colorScheme),
            cardTheme: cardTheme(),
            listTileTheme: listTileTheme(colorScheme),
            bottomAppBarTheme: bottomAppBarTheme(colorScheme),
            bottomNavigationBarTheme: bottomNavigationBarTheme(colorScheme),
            navigationRailTheme: navigationRailTheme(colorScheme),
            tabBarTheme: tabBarTheme(colorScheme),
            drawerTheme: drawerTheme(colorScheme),
            dropdownMenuTheme: dropdownMenuTheme(colorScheme),
            navigationBarTheme: navigationBarTheme(colorScheme),
            buttonTheme: buttonTheme(colorScheme),
            scaffoldBackgroundColor: colorScheme.surface,
            inputDecorationTheme: inputDecorationTheme(colorScheme));
  }

  ThemeData dark([final Color? targetColor]) {
    final colorScheme = colors(Brightness.dark, targetColor);
    return ThemeData(
            brightness: Brightness.dark,
            useMaterial3: true,
            fontFamily: "Aileron")
        .copyWith(
            // Add page transitions
            pageTransitionsTheme: pageTransitionsTheme,
            colorScheme: colorScheme,
            appBarTheme: appBarTheme(colorScheme),
            cardTheme: cardTheme(),
            listTileTheme: listTileTheme(colorScheme),
            bottomAppBarTheme: bottomAppBarTheme(colorScheme),
            bottomNavigationBarTheme: bottomNavigationBarTheme(colorScheme),
            navigationBarTheme: navigationBarTheme(colorScheme),
            navigationRailTheme: navigationRailTheme(colorScheme),
            tabBarTheme: tabBarTheme(colorScheme),
            dropdownMenuTheme: dropdownMenuTheme(colorScheme),
            drawerTheme: drawerTheme(colorScheme),
            buttonTheme: buttonTheme(colorScheme),
            scaffoldBackgroundColor: colorScheme.surface,
            inputDecorationTheme: inputDecorationTheme(colorScheme));
  }

  InputDecorationTheme inputDecorationTheme(final ColorScheme colorScheme) =>
      InputDecorationTheme(
        filled: true,
        fillColor: colorScheme.surfaceContainer,
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16))),
      );

  ThemeMode themeMode() {
    return settings.value.themeMode;
  }

  ThemeData theme(final BuildContext context, [final Color? targetColor]) {
    final brightness = MediaQuery.of(context).platformBrightness;
    return brightness == Brightness.light
        ? light(targetColor)
        : dark(targetColor);
  }

  static ThemeProvider of(final BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ThemeProvider>()!;
  }

  @override
  bool updateShouldNotify(covariant final ThemeProvider oldWidget) {
    return oldWidget.settings != settings;
  }
}

class ThemeSettings {
  ThemeSettings({
    required this.sourceColor,
    required this.themeMode,
  });

  final Color sourceColor;
  final ThemeMode themeMode;
}

// Custom Colors
const linkColor = CustomColor(
  name: 'Link Color',
  color: Colors.teal,
);

class CustomColor {
  const CustomColor({
    required this.name,
    required this.color,
    this.blend = true,
  });

  final String name;
  final Color color;
  final bool blend;

  Color value(final ThemeProvider provider) {
    return provider.custom(this);
  }
}
