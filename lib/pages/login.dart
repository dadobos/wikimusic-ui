import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/http/user.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  var loginControllers = Map.fromEntries([
    MapEntry("email", TextEditingController()),
    MapEntry("password", TextEditingController())
  ]);

  var resetControllers = Map.fromEntries([
    MapEntry("email", TextEditingController()),
  ]);

  @override
  Widget build(final BuildContext context) {
    final loginFormWidget = SizedBox(
      width:
          deviceWidth(context) > 800 ? deviceWidth(context, scale: 0.3) : null,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          verticalSpacer(),
          header2(context, t(context).routeNames.login),
          verticalSpacer(),
          requiredField(loginControllers["email"],
              validator: emailValidator,
              label: "Enter your email address",
              errorMessage: 'Please enter a valid email address'),
          verticalSpacer(),
          passwordField(loginControllers["password"],
              label: "Enter your password",
              errorMessage: 'Please enter a valid password'),
        ],
      ),
    );

    final resetPasswordWidget = SizedBox(
      width:
          deviceWidth(context) > 800 ? deviceWidth(context, scale: 0.3) : null,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          verticalSpacer(),
          header2(context, t(context).titles.resetPassword),
          verticalSpacer(),
          requiredField(resetControllers["email"]!,
              validator: emailValidator,
              label: "Enter your email address",
              errorMessage: 'Please enter a valid email address'),
        ],
      ),
    );
    return MyPage(
        env: widget.env,
        context: context,
        title: widget.title,
        routeName: "login",
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          padding(Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              verticalSpacer(),
              header1(
                context,
                t(context).titles.login,
              ),
              Column(children: [
                verticalSpacer(),
                const Text(
                    "Log in to the one and only music community which is free as in freedom!"),
                verticalSpacer(),
                languageMenu(context, widget.env),
                verticalSpacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    wrap(children: [
                      maybeDrawFuture(
                          UserPreferencesProvider.hasPreviousSession(
                              widget.env),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              wrap(children: [
                                primaryButton(() async {
                                  // ignore: use_build_context_synchronously
                                  context.go("/songs");
                                }, "continue session"),
                                primaryButton(() async {
                                  await UserPreferencesProvider.logout();
                                  // ignore: use_build_context_synchronously
                                  context.go("/login");
                                }, "log out"),
                              ])
                            ],
                          ))
                    ])
                  ],
                ),
                verticalSpacer(),
                wrap(children: [
                  ValidatedForm(
                    env: widget.env,
                    onSubmit: () async {
                      await doLoginCall(
                          widget.env,
                          loginControllers["email"]!.text,
                          loginControllers["password"]!.text);
                      await meCall(widget.env);
                      // ignore: use_build_context_synchronously
                      context.go("/songs");
                    },
                    widget: loginFormWidget,
                    controllers: loginControllers,
                  ),
                  ValidatedForm(
                    env: widget.env,
                    onSubmit: () async {
                      await doResetPasswordCall(
                          widget.env, resetControllers["email"]!.text);
                    },
                    widget: resetPasswordWidget,
                    submitText: t(context).buttons.reset,
                    controllers: resetControllers,
                  )
                ])
              ]),
            ],
          )),
        ]));
  }
}
