// import 'package:encrypt_shared_preferences/provider.dart';
import 'package:flutter/material.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<AboutPage> createState() => AboutPageState();
}

class AboutPageState extends State<AboutPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(final BuildContext context) {
    final Widget w = Column(
      children: [
        scrollableArea([
          Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  header1(context, widget.title),
                  verticalSpacer(),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: wrap(children: [
                      const Text(
                          "WikiMusic as a software means an ingenious combination of a backend, a REST API, a database, and a user-interface to create a Content Management System (CMS) and a Gallery for music of all kinds, with educational intents.")
                    ]),
                  ),
                ],
              ),
            ),
          )
        ])
      ],
    );

    return MyPage(
        env: widget.env,
        context: context,
        routeName: "about",
        title: widget.title,
        isLoggedIn: widget.env.authToken != null,
        child: w);
  }
}
