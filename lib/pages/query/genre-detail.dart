import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/genres.dart';
import 'package:wikimusic_ui/model/genres.dart';

class GenreDetailPage extends StatefulWidget {
  const GenreDetailPage({
    super.key,
    required this.title,
    required this.env,
    required this.identifier,
  });

  final String title;
  final String identifier;
  final Env env;

  @override
  State<GenreDetailPage> createState() => GenreDetailPageState();
}

class GenreDetailPageState extends State<GenreDetailPage> {
  Future<Genre>? genre;
  String? selectedC;
  String? selectedF = "L";

  @override
  void initState() {
    super.initState();

    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      genre = fetchGenreByIdentifier(widget.env, widget.identifier);
    });
  }

  FutureBuilder<Genre> makeGenrePage(final Future<Genre>? genre) {
    return FutureBuilder<Genre>(
      future: genre,
      builder: (final context, final snapshot) {
        if (snapshot.hasData && genre != null) {
          final genre = snapshot.data!;
          return makeNiceGenrePage(context, genre);
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Widget makeNiceGenrePage(final BuildContext context, final Genre genre) {
    final sortedArtworks = genre.artworks.values.toList();
    sortedArtworks.sort((final a, final b) =>
        a.artwork.orderValue.compareTo(b.artwork.orderValue));

    final entityButtons = SizedBox(
        width: (MediaQuery.of(context).size.width * 0.6),
        child: Column(
          children: [
            wrap(
              children: [
                ...makeButtonsFromOpinions(
                    genre.opinions.values
                        .where((final e) => e.opinion.isLike)
                        .length,
                    genre.opinions.values
                        .where((final e) => e.opinion.isDislike)
                        .length, onLike: () async {
                  await doHttp(
                      widget.env, "${widget.env.apiUrl}/genres/opinions",
                      method: "POST",
                      authToken: widget.env.authToken,
                      data: {
                        "genreOpinions": [
                          {
                            "genreIdentifier": widget.identifier,
                            "isLike": true,
                          }
                        ]
                      });
                  refetchEntity();
                }, onDislike: () async {
                  await doHttp(
                      widget.env, "${widget.env.apiUrl}/genres/opinions",
                      method: "POST",
                      authToken: widget.env.authToken,
                      data: {
                        "genreOpinions": [
                          {
                            "genreIdentifier": widget.identifier,
                            "isLike": false,
                          }
                        ]
                      });
                  refetchEntity();
                }),
                primaryButton(
                    () => context
                        .go("/management/genres/edit/${widget.identifier}"),
                    "edit"),
                deleteButton(
                  context,
                  "Delete genre?",
                  () async {
                    await doHttp(widget.env,
                        "${widget.env.apiUrl}/genres/${widget.identifier}",
                        method: "DELETE", authToken: widget.env.authToken);
                    // ignore: use_build_context_synchronously
                    context.go("/genres");
                  },
                )
              ],
            )
          ],
        ));

    final entityTable = twoColTable(rows: [
      twoColRow(
          context, "view count", "${genre.viewCount} ${t(context).views}"),
      twoColRow(context, "created at", "${genre.createdAt.toLocal()}"),
      if (genre.lastEditedAt != null)
        twoColRow(
            context, "last edited at", "${genre.lastEditedAt!.toLocal()}"),
      twoColRowWidget(
          context,
          "identifier (UUID v4)",
          Row(children: [
            TextButton(
                child: const Icon(
                  Icons.copy,
                  size: 16,
                ),
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: genre.identifier));
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: Text(
                            'Copied ${genre.displayName} identifier (UUID v4) to clipboard - ${genre.identifier}')),
                  );
                })
          ])),
      if (genre.spotifyUrl != null)
        twoColRowWidget(context, "Spotify",
            link(genre.spotifyUrl!, label: "${genre.displayName} 🔗 Spotify")),
      if (genre.wikipediaUrl != null)
        twoColRowWidget(
            context,
            "Wikipedia",
            link(genre.wikipediaUrl!,
                label: "${genre.displayName} 🔗 Wikipedia")),
      if (genre.youtubeUrl != null)
        twoColRowWidget(context, "YouTube",
            link(genre.youtubeUrl!, label: "${genre.displayName} 🔗 YouTube")),
      if (genre.soundcloudUrl != null)
        twoColRowWidget(
            context,
            "Soundcloud",
            link(genre.soundcloudUrl!,
                label: "${genre.displayName} 🔗 Soundcloud")),
    ]);
    final entityDescription = padding(
        Column(children: [
          if (genre.description != null) Text(genre.description!)
        ]),
        insets: 50);
    return SingleChildScrollView(
      physics: const ScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          verticalSpacer(),
          header1(context, genre.displayName),
          padding(wrap(children: [
            SizedBox(
                width: deviceWidth(context) > 800
                    ? deviceWidth(context, scale: 0.4)
                    : null,
                child: Column(children: [
                  makeImageOrCarousel(
                      context,
                      sortedArtworks
                          .map((final e) => e.artwork.contentUrl)
                          .toList()),
                  entityDescription
                ])),
            SizedBox(
                width: deviceWidth(context) > 800
                    ? deviceWidth(context, scale: 0.4)
                    : null,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      entityButtons,
                      verticalSpacer(),
                      entityTable,
                    ])),
          ])),
        ],
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return MyPage(
        env: widget.env,
        context: context,
        title: widget.title,
        hasBackButton: true,
        routeName: "genres",
        backButtonDestination: "/genres",
        isLoggedIn: widget.env.authToken != null,
        child: makeGenrePage(genre));
  }
}
