import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/components/search-bar.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/http/songs.dart';
import 'package:wikimusic_ui/model/app.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongListPage extends StatefulWidget {
  const SongListPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<SongListPage> createState() => SongListPageState();
}

class SongListPageState extends State<SongListPage> {
  String? searchData;
  PagingController<int, Song> pagingController =
      PagingController(firstPageKey: 0);

  Future<void> fetchPage(
      final Env env, final SortOrder sorting, final int pageKey) async {
    try {
      GetSongsQueryResponse songs;
      if (searchData == null || searchData == "") {
        songs = await fetchSongs(env, sorting, (pageKey));
      } else {
        songs = await searchSongs(env, sorting, (pageKey), searchData);
      }
      const pageSize = 10;
      final newItems =
          songs.sortOrder.map((final e) => songs.songs[e]!).toList();
      final isLastPage = newItems.length < pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  @override
  void initState() {
    pagingController.addPageRequestListener((final pageKey) {
      fetchPage(
        widget.env,
        UserPreferencesProvider.of(context).userPreferences.value.songSorting,
        pageKey,
      );
    });
    super.initState();
  }

  @override
  void dispose() {
    pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final menu = Padding(
      padding: const EdgeInsets.all(20.0),
      child: Align(
          alignment: Alignment.centerRight,
          child: DropdownMenu<SortOrder>(
              initialSelection: UserPreferencesProvider.of(context)
                  .userPreferences
                  .value
                  .songSorting,
              onSelected: (final SortOrder? value) async {
                UserPreferencesProvider.of(context).userPreferences.value =
                    UserPreferencesProvider.of(context)
                        .userPreferences
                        .value
                        .withSongSorting(value!);
                await UserPreferencesProvider.of(context)
                    .userPreferences
                    .value
                    .toStorage();
                setState(() {
                  pagingController.refresh();
                });
              },
              dropdownMenuEntries:
                  sorts.entries.map<DropdownMenuEntry<SortOrder>>((final e) {
                return DropdownMenuEntry<SortOrder>(
                    value: e.value, label: e.key);
              }).toList())),
    );

    return MyPage(
        env: widget.env,
        context: context,
        title: widget.title,
        isLoggedIn: widget.env.authToken != null,
        routeName: "songs",
        child: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              verticalSpacer(),
              padding(
                wrap(children: [
                  menu,
                  WikiSearchBar(onChanged: (final value) {
                    Debounce(const Duration(milliseconds: 500))(() {
                      searchData = value;
                      pagingController.refresh();
                    });
                  })
                ]),
              ),
              verticalSpacer(),
              PagedListView<int, Song>(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(4),
                  pagingController: pagingController,
                  builderDelegate: PagedChildBuilderDelegate<Song>(
                    itemBuilder: (final context, final item, final index) {
                      final images = item.artworks.values.toList();
                      images.sort((final a, final b) =>
                          a.artwork.orderValue.compareTo(b.artwork.orderValue));
                      final likes = item.opinions.values
                          .where((final e) => e.opinion.isLike)
                          .length;
                      final dislikes = item.opinions.values
                          .where((final e) => e.opinion.isDislike)
                          .length;

                      return makeListCard(
                          context,
                          likes,
                          dislikes,
                          images.firstOrNull?.artwork.contentUrl,
                          item.identifier,
                          item.displayName,
                          meta: item.artists.values.isNotEmpty
                              ? item.artists.values.join(' & ')
                              : null,
                          item.viewCount, () {
                        context.go("/songs/${item.identifier}");
                      });
                    },
                  )),
            ],
          ),
        ));
  }
}
