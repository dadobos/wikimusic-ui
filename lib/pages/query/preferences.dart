// import 'package:encrypt_shared_preferences/provider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/brightness_toggle.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/http/user.dart';
import 'package:wikimusic_ui/model/login.dart';

class PreferencesPage extends StatefulWidget {
  const PreferencesPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<PreferencesPage> createState() => PreferencesPageState();
}

class PreferencesPageState extends State<PreferencesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(final BuildContext context) {
    return MyPage(
        env: widget.env,
        context: context,
        routeName: "preferences",
        title: widget.title,
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Center(
            child: Column(
              children: [
                header1(context, t(context).routeNames.preferences),
                verticalSpacer(),
                meStatusBuilder(context)
              ],
            ),
          )
        ]));
  }

  Future<void> onLogout(final BuildContext context) async {
    await UserPreferencesProvider.logout();
    // ignore: use_build_context_synchronously
    context.go("/login");
  }

  Widget meStatusBuilder(final BuildContext context) {
    return FutureBuilder(
        future: meCall(widget.env),
        builder: (final context, final snapshot) {
          if (snapshot.hasData) {
            final user = snapshot.data!;
            final mappedRoles = user.roles
                .map(
                  (final e) =>
                      UserRole.values.firstWhere((final x) => x.roleName == e),
                )
                .toList();

            return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      wrap(children: [
                        primaryButton(
                            () => context.go("/login"), "Log in as other"),
                        primaryButton(() => onLogout(context), "Log out"),
                        const BrightnessToggle(),
                      ]),
                    ],
                  ),
                  verticalSpacer(height: 30),
                  languageMenu(context, widget.env),
                  verticalSpacer(height: 30),
                  const Divider(),
                  verticalSpacer(height: 30),
                  header2(context, "About the current user"),
                  verticalSpacer(),
                  twoColTable(rows: [
                    twoColRow(context, "name", user.displayName),
                    twoColRow(context, "email", user.emailAddress),
                    twoColRow(context, "roles", "$mappedRoles"),
                    twoColRow(context, "identifier", user.identifier)
                  ]),
                  verticalSpacer(),
                ]);
          } else if (snapshot.hasError) {
            Logger("login page").log(Level.SEVERE, "${snapshot.error}");
            return Text("${snapshot.error}");
          } else {
            return const CircularProgressIndicator();
          }
        });
  }
}
