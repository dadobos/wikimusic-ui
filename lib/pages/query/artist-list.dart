import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/components/search-bar.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/http/artists.dart';
import 'package:wikimusic_ui/model/app.dart';
import 'package:wikimusic_ui/model/artists.dart';

class ArtistListPage extends StatefulWidget {
  const ArtistListPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<ArtistListPage> createState() => ArtistListPageState();
}

class ArtistListPageState extends State<ArtistListPage> {
  String? searchData;
  PagingController<int, Artist> pagingController =
      PagingController(firstPageKey: 0);

  Future<void> fetchPage(
      final Env env, final SortOrder sorting, final int pageKey) async {
    try {
      GetArtistsQueryResponse artists;
      if (searchData == null || searchData == "") {
        artists = await fetchArtists(env, sorting, (pageKey));
      } else {
        artists = await searchArtists(env, sorting, (pageKey), searchData);
      }
      const pageSize = 10;
      final newItems =
          artists.sortOrder.map((final e) => artists.artists[e]!).toList();
      final isLastPage = newItems.length < pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      if (kDebugMode) {
        print(error);
      }
      pagingController.error = error;
    }
  }

  @override
  void initState() {
    pagingController.addPageRequestListener((final pageKey) {
      fetchPage(
        widget.env,
        UserPreferencesProvider.of(context).userPreferences.value.songSorting,
        pageKey,
      );
    });
    super.initState();
  }

  @override
  void dispose() {
    pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final menu = Padding(
      padding: const EdgeInsets.all(20.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: DropdownMenu<SortOrder>(
            initialSelection: UserPreferencesProvider.of(context)
                .userPreferences
                .value
                .artistSorting,
            onSelected: (final SortOrder? value) async {
              UserPreferencesProvider.of(context).userPreferences.value =
                  UserPreferencesProvider.of(context)
                      .userPreferences
                      .value
                      .withArtistSorting(value!);
              await UserPreferencesProvider.of(context)
                  .userPreferences
                  .value
                  .toStorage();
              setState(() {
                pagingController.refresh();
              });
            },
            dropdownMenuEntries:
                sorts.entries.map<DropdownMenuEntry<SortOrder>>((final e) {
              return DropdownMenuEntry<SortOrder>(value: e.value, label: e.key);
            }).toList()),
      ),
    );

    return MyPage(
        env: widget.env,
        context: context,
        title: widget.title,
        routeName: "artists",
        isLoggedIn: widget.env.authToken != null,
        child: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              verticalSpacer(),
              padding(
                wrap(children: [
                  menu,
                  WikiSearchBar(onChanged: (final value) {
                    Debounce(const Duration(milliseconds: 500))(() {
                      searchData = value;
                      pagingController.refresh();
                    });
                  })
                ]),
              ),
              verticalSpacer(),
              PagedListView<int, Artist>(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(4),
                pagingController: pagingController,
                builderDelegate: PagedChildBuilderDelegate<Artist>(
                    itemBuilder: (final context, final item, final index) {
                  final images = item.artworks.values.toList();
                  images.sort((final a, final b) =>
                      a.artwork.orderValue.compareTo(b.artwork.orderValue));
                  final likes = item.opinions.values
                      .where((final e) => e.opinion.isLike)
                      .length;
                  final dislikes = item.opinions.values
                      .where((final e) => e.opinion.isDislike)
                      .length;

                  return makeListCard(
                      context,
                      likes,
                      dislikes,
                      images.firstOrNull?.artwork.contentUrl,
                      item.identifier,
                      item.displayName,
                      item.viewCount, () {
                    context.go("/artists/${item.identifier}");
                  });
                }),
              )
            ],
          ),
        ));
  }
}
