import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/artists.dart';
import 'package:wikimusic_ui/model/artists.dart';

class ArtistDetailPage extends StatefulWidget {
  const ArtistDetailPage({
    super.key,
    required this.title,
    required this.env,
    required this.identifier,
  });

  final String title;
  final String identifier;
  final Env env;

  @override
  State<ArtistDetailPage> createState() => ArtistDetailPageState();
}

class ArtistDetailPageState extends State<ArtistDetailPage> {
  Future<Artist>? artist;
  String? selectedC;
  String? selectedF = "L";

  @override
  void initState() {
    super.initState();

    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      artist = fetchArtistByIdentifier(widget.env, widget.identifier);
    });
  }

  FutureBuilder<Artist> makeArtistPage(final Future<Artist>? artist) {
    return FutureBuilder<Artist>(
      future: artist,
      builder: (final context, final snapshot) {
        if (snapshot.hasData && artist != null) {
          final artist = snapshot.data!;
          return makeNiceArtistPage(context, artist);
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Widget makeNiceArtistPage(final BuildContext context, final Artist artist) {
    final sortedArtworks = artist.artworks.values.toList();
    sortedArtworks.sort((final a, final b) =>
        a.artwork.orderValue.compareTo(b.artwork.orderValue));

    final entityButtons = SizedBox(
        width: (MediaQuery.of(context).size.width * 0.6),
        child: Column(
          children: [
            wrap(
              children: [
                ...makeButtonsFromOpinions(
                    artist.opinions.values
                        .where((final e) => e.opinion.isLike)
                        .length,
                    artist.opinions.values
                        .where((final e) => e.opinion.isDislike)
                        .length, onLike: () async {
                  await doHttp(
                      widget.env, "${widget.env.apiUrl}/artists/opinions",
                      method: "POST",
                      authToken: widget.env.authToken,
                      data: {
                        "artistOpinions": [
                          {
                            "artistIdentifier": widget.identifier,
                            "isLike": true,
                          }
                        ]
                      });
                  refetchEntity();
                }, onDislike: () async {
                  await doHttp(
                      widget.env, "${widget.env.apiUrl}/artists/opinions",
                      method: "POST",
                      authToken: widget.env.authToken,
                      data: {
                        "artistOpinions": [
                          {
                            "artistIdentifier": widget.identifier,
                            "isLike": false,
                          }
                        ]
                      });
                  refetchEntity();
                }),
                primaryButton(
                    () => context
                        .go("/management/artists/edit/${widget.identifier}"),
                    "edit"),
                deleteButton(
                  context,
                  "Delete artist?",
                  () async {
                    await doHttp(widget.env,
                        "${widget.env.apiUrl}/artists/${widget.identifier}",
                        method: "DELETE", authToken: widget.env.authToken);
                    // ignore: use_build_context_synchronously
                    context.go("/artists");
                  },
                )
              ],
            )
          ],
        ));

    final entityTable = twoColTable(rows: [
      twoColRow(
          context, "view count", "${artist.viewCount} ${t(context).views}"),
      twoColRow(context, "created at", "${artist.createdAt.toLocal()}"),
      if (artist.lastEditedAt != null)
        twoColRow(
            context, "last edited at", "${artist.lastEditedAt!.toLocal()}"),
      twoColRowWidget(
          context,
          "identifier (UUID v4)",
          Row(children: [
            TextButton(
                child: const Icon(
                  Icons.copy,
                  size: 16,
                ),
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: artist.identifier));
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: Text(
                            'Copied ${artist.displayName} identifier (UUID v4) to clipboard - ${artist.identifier}')),
                  );
                })
          ])),
      if (artist.spotifyUrl != null)
        twoColRowWidget(
            context,
            "Spotify",
            link(artist.spotifyUrl!,
                label: "${artist.displayName} 🔗 Spotify")),
      if (artist.wikipediaUrl != null)
        twoColRowWidget(
            context,
            "Wikipedia",
            link(artist.wikipediaUrl!,
                label: "${artist.displayName} 🔗  Wikipedia")),
      if (artist.youtubeUrl != null)
        twoColRowWidget(
            context,
            "YouTube",
            link(artist.youtubeUrl!,
                label: "${artist.displayName} 🔗 YouTube")),
      if (artist.soundcloudUrl != null)
        twoColRowWidget(
            context,
            "Soundcloud",
            link(artist.soundcloudUrl!,
                label: "${artist.displayName} 🔗 Soundcloud")),
    ]);
    final entityDescription = padding(
        Column(children: [
          if (artist.description != null) Text(artist.description!)
        ]),
        insets: 50);

    return SingleChildScrollView(
      physics: const ScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          verticalSpacer(),
          header1(context, artist.displayName),
          verticalSpacer(),
          padding(wrap(children: [
            SizedBox(
                width: deviceWidth(context) > 800
                    ? deviceWidth(context, scale: 0.4)
                    : null,
                child: Column(children: [
                  makeImageOrCarousel(
                      context,
                      sortedArtworks
                          .map((final e) => e.artwork.contentUrl)
                          .toList()),
                  entityDescription
                ])),
            SizedBox(
                width: deviceWidth(context) > 800
                    ? deviceWidth(context, scale: 0.4)
                    : null,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      entityButtons,
                      verticalSpacer(),
                      entityTable,
                    ])),
          ])),
        ],
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return MyPage(
        env: widget.env,
        context: context,
        routeName: "artists",
        title: widget.title,
        hasBackButton: true,
        backButtonDestination: "/artists",
        isLoggedIn: widget.env.authToken != null,
        child: makeArtistPage(artist));
  }
}
