import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/components/songs.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/songs.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongDetailPage extends StatefulWidget {
  const SongDetailPage({
    super.key,
    required this.title,
    required this.env,
    required this.identifier,
  });

  final String title;
  final String identifier;
  final Env env;

  @override
  State<SongDetailPage> createState() => SongDetailPageState();
}

class SongDetailPageState extends State<SongDetailPage> {
  Future<Song>? song;
  String? selectedF = "M";

  @override
  void initState() {
    super.initState();

    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      song = fetchSongByIdentifier(widget.env, widget.identifier);
    });
  }

  FutureBuilder<Song> makeSongPage(final Future<Song>? song) {
    return FutureBuilder<Song>(
      future: song,
      builder: (final context, final snapshot) {
        if (snapshot.hasData && song != null) {
          final song = snapshot.data!;
          return makeNiceSongPage(context, song);
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Widget makeNiceSongPage(final BuildContext context, final Song song) {
    final sortedArtworks = song.artworks.values.toList();
    sortedArtworks.sort((final a, final b) =>
        a.artwork.orderValue.compareTo(b.artwork.orderValue));

    final entityButtons = SizedBox(
      width: (MediaQuery.of(context).size.width * 0.6),
      child: Column(
        children: [
          wrap(
            children: [
              ...makeButtonsFromOpinions(
                  song.opinions.values
                      .where((final e) => e.opinion.isLike)
                      .length,
                  song.opinions.values
                      .where((final e) => e.opinion.isDislike)
                      .length, onLike: () async {
                await doHttp(widget.env, "${widget.env.apiUrl}/songs/opinions",
                    method: "POST",
                    authToken: widget.env.authToken,
                    data: {
                      "songOpinions": [
                        {
                          "songIdentifier": widget.identifier,
                          "isLike": true,
                        }
                      ]
                    });
                refetchEntity();
              }, onDislike: () async {
                await doHttp(widget.env, "${widget.env.apiUrl}/songs/opinions",
                    method: "POST",
                    authToken: widget.env.authToken,
                    data: {
                      "songOpinions": [
                        {
                          "songIdentifier": widget.identifier,
                          "isLike": false,
                        }
                      ]
                    });
                refetchEntity();
              }),
              primaryButton(
                  () =>
                      context.go("/management/songs/edit/${widget.identifier}"),
                  "edit"),
              drawForMaintainers(deleteButton(
                context,
                "Delete song?",
                () async {
                  await doHttp(widget.env,
                      "${widget.env.apiUrl}/songs/${widget.identifier}",
                      method: "DELETE", authToken: widget.env.authToken);
                  // ignore: use_build_context_synchronously
                  context.go("/songs");
                },
              )),
              primaryButton(() {
                context
                    .go("/management/songs/contents/add/${widget.identifier}");
              }, "+ version"),
            ],
          )
        ],
      ),
    );
    final entityTable = twoColTable(rows: [
      twoColRow(context, "view count", "${song.viewCount} ${t(context).views}"),
      twoColRow(context, "created at", "${song.createdAt.toLocal()}"),
      if (song.lastEditedAt != null)
        twoColRow(context, "last edited at", "${song.lastEditedAt!.toLocal()}"),
      twoColRowWidget(
          context,
          "identifier (UUID v4)",
          Row(children: [
            TextButton(
                child: const Icon(
                  Icons.copy,
                  size: 16,
                ),
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: song.identifier));
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: Text(
                            'Copied ${song.displayName} identifier (UUID v4) to clipboard - ${song.identifier}')),
                  );
                })
          ])),
      if (song.musicKey != null)
        twoColRow(context, "music key", song.musicKey!),
      if (song.musicTuning != null)
        twoColRow(context, "music tuning", song.musicTuning!),
      if (song.musicCreationDate != null)
        twoColRow(context, "music creation date", song.musicCreationDate!),
      if (song.albumName != null)
        twoColRow(context, "album name", song.albumName!),
      if (song.albumInfoLink != null)
        twoColRowWidget(context, "album info",
            link(song.albumInfoLink!, label: "More info on the album")),
      if (song.spotifyUrl != null)
        twoColRowWidget(context, "Spotify",
            link(song.spotifyUrl!, label: "${song.displayName} 🔗 Spotify")),
      if (song.wikipediaUrl != null)
        twoColRowWidget(
            context,
            "Wikipedia",
            link(song.wikipediaUrl!,
                label: "${song.displayName} 🔗 Wikipedia")),
      if (song.youtubeUrl != null)
        twoColRowWidget(context, "YouTube",
            link(song.youtubeUrl!, label: "${song.displayName} 🔗 YouTube")),
      if (song.soundcloudUrl != null)
        twoColRowWidget(
            context,
            "Soundcloud",
            link(song.soundcloudUrl!,
                label: "${song.displayName} 🔗 Soundcloud")),
    ]);
    final entityDescription = padding(
        Column(
            children: [if (song.description != null) Text(song.description!)]),
        insets: 50);
    return SingleChildScrollView(
      physics: const ScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          verticalSpacer(),
          header1(context, song.displayName),
          verticalSpacer(),
          if (song.artists.values.isNotEmpty)
            header2(context, "by ${song.artists.values.join(' & ')}"),
          verticalSpacer(),
          padding(
            wrap(children: [
              SizedBox(
                  width: deviceWidth(context) > 800
                      ? deviceWidth(context, scale: 0.4)
                      : null,
                  child: Column(children: [
                    makeImageOrCarousel(
                        context,
                        sortedArtworks
                            .map((final e) => e.artwork.contentUrl)
                            .toList()),
                    entityDescription
                  ])),
              SizedBox(
                width: deviceWidth(context) > 800
                    ? deviceWidth(context, scale: 0.4)
                    : null,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    entityButtons,
                    verticalSpacer(),
                    entityTable,
                  ],
                ),
              ),
            ]),
          ),
          verticalSpacer(),
          SongVersionView(
            env: widget.env,
            song: song,
            refetchEntity: refetchEntity,
          )
        ],
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return MyPage(
        env: widget.env,
        context: context,
        routeName: "songs",
        title: widget.title,
        hasBackButton: true,
        backButtonDestination: "/songs",
        isLoggedIn: widget.env.authToken != null,
        child: makeSongPage(song));
  }
}
