// import 'package:encrypt_shared_preferences/provider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';

class ManagementPage extends StatefulWidget {
  const ManagementPage({super.key, required this.title, required this.env});

  final String title;
  final Env env;

  @override
  State<ManagementPage> createState() => ManagementPageState();
}

class ManagementPageState extends State<ManagementPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(final BuildContext context) {
    final Widget w = Column(
      children: [
        scrollableArea([
          Center(
            child: Column(
              children: [
                header1(context, widget.title),
                verticalSpacer(height: 60),
                wrap(children: [
                  primaryButton(() {
                    context.go("/management/songs/add");
                  }, "Add a song"),
                  primaryButton(() {
                    context.go("/management/artists/add");
                  }, "Add an artist"),
                  primaryButton(() {
                    context.go("/management/genres/add");
                  }, "Add a genre"),
                ]),
                verticalSpacer(height: 30),
                const Divider(),
                verticalSpacer(height: 30),
                wrap(children: [
                  primaryButton(() {
                    context.go("/management/users/add");
                  }, "Invite user"),
                  primaryButton(() {
                    context.go("/management/users/delete");
                  }, "Delete user")
                ])
              ],
            ),
          )
        ])
      ],
    );

    return MyPage(
        env: widget.env,
        context: context,
        routeName: "management",
        title: widget.title,
        isLoggedIn: widget.env.authToken != null,
        child: w);
  }
}
