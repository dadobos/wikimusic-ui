import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/users.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({super.key, required this.env, required this.token});

  final Env env;
  final String token;

  @override
  State<ResetPasswordPage> createState() => ResetPasswordPageState();
}

class ResetPasswordPageState extends State<ResetPasswordPage> {
  final controllers = Map.fromEntries([
    MapEntry("email", TextEditingController()),
    MapEntry("password", TextEditingController()),
    MapEntry("passwordConfirm", TextEditingController()),
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Perform a password reset!"),
        verticalSpacer(),
        requiredField(controllers["email"],
            validator: emailValidator,
            label: "Enter the email of the user",
            errorMessage: "Please enter a valid email"),
        passwordField(controllers["password"],
            label: "Enter the new password",
            errorMessage: "Please enter a valid password"),
        passwordField(controllers["passwordConfirm"],
            label: "Confirm the password",
            errorMessage: "Please enter a valid password"),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = DoPasswordResetRequest(
        email: fromTextController(controllers["email"]) ?? "",
        token: widget.token,
        password: fromTextController(controllers["password"]) ?? "",
        passwordConfirm:
            fromTextController(controllers["passwordConfirm"]) ?? "",
      );
      await doHttp(widget.env, "${widget.env.apiUrl}/reset-password/do",
          method: "POST", data: r.toJson());

      // ignore: use_build_context_synchronously
      context.go("/login");
    }

    return MyPage(
        env: widget.env,
        context: context,
        title: "Reset a password",
        routeName: "management",
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Column(
            children: [
              header1(context, "Reset Password"),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          ),
        ]));
  }
}
