import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongContentAddPage extends StatefulWidget {
  const SongContentAddPage({
    super.key,
    required this.env,
    required this.songIdentifier,
  });

  final Env env;
  final String songIdentifier;

  @override
  State<SongContentAddPage> createState() => SongContentAddPageState();
}

class SongContentAddPageState extends State<SongContentAddPage> {
  final controllers = Map.fromEntries([
    MapEntry("versionName", TextEditingController()),
    MapEntry("instrumentType", TextEditingController()),
    MapEntry("asciiLegend", TextEditingController()),
    MapEntry("asciiContents", TextEditingController()),
    MapEntry("pdfContents", TextEditingController()),
    MapEntry("guitarPro", TextEditingController())
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Contribute to the WikiMusic knowledge base!"),
        verticalSpacer(),
        requiredField(controllers["versionName"],
            label: "Enter the name of the version",
            errorMessage: "Please enter a valid name"),
        verticalSpacer(),
        requiredField(controllers["instrumentType"],
            label: "Enter an instrument type",
            errorMessage: "Please enter a valid instrument type"),
        verticalSpacer(),
        optionalField(controllers["asciiLegend"],
            label: "Enter ASCII legend",
            errorMessage: "Please enter a valid ASCII legend",
            keyboardType: TextInputType.multiline,
            minLines: 6,
            style: GoogleFonts.robotoMono(
                textStyle: const TextStyle(
              fontWeight: FontWeight.w500,
            ))),
        verticalSpacer(),
        optionalField(controllers["asciiContents"],
            label: "Enter ASCII contents",
            errorMessage: "Please enter a valid ASCII contents",
            keyboardType: TextInputType.multiline,
            minLines: 12,
            style: GoogleFonts.robotoMono(
                textStyle: const TextStyle(
              fontWeight: FontWeight.w500,
            ))),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = InsertSongContentsRequest(songContents: [
        InsertSongContentsRequestItem(
          songIdentifier: widget.songIdentifier,
          versionName: fromTextController(controllers["versionName"]) ?? "",
          instrumentType: fromTextController(controllers["instrumentType"]),
          asciiLegend: fromTextController(controllers["asciiLegend"]),
          asciiContents: fromTextController(controllers["asciiContents"]),
        )
      ]);
      await doHttp(widget.env, "${widget.env.apiUrl}/songs/contents",
          method: "POST", data: r.toJson(), authToken: widget.env.authToken);

      // ignore: use_build_context_synchronously
      context.go("/songs/${widget.songIdentifier}");
    }

    return MyPage(
        env: widget.env,
        context: context,
        title: t(context).routeNames.newSongContent,
        isLoggedIn: widget.env.authToken != null,
        routeName: "management",
        child: scrollableArea([
          Column(
            children: [
              header1(context, t(context).routeNames.newSongContent),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          ),
        ]));
  }
}
