import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongAddPage extends StatefulWidget {
  const SongAddPage({super.key, required this.env});

  final Env env;

  @override
  State<SongAddPage> createState() => SongAddPageState();
}

class SongAddPageState extends State<SongAddPage> {
  final controllers = Map.fromEntries([
    MapEntry("displayName", TextEditingController()),
    MapEntry("wikipediaUrl", TextEditingController()),
    MapEntry("spotifyUrl", TextEditingController()),
    MapEntry("youtubeUrl", TextEditingController()),
    MapEntry("soundcloudUrl", TextEditingController()),
    MapEntry("musicKey", TextEditingController()),
    MapEntry("tuning", TextEditingController()),
    MapEntry("musicCreationDate", TextEditingController()),
    MapEntry("albumName", TextEditingController()),
    MapEntry("albumInfo", TextEditingController()),
    MapEntry("description", TextEditingController())
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Contribute to the WikiMusic knowledge base!"),
        verticalSpacer(),
        requiredField(controllers["displayName"],
            label: "Enter the name of the song",
            errorMessage: "Please enter a valid name"),
        verticalSpacer(),
        optionalField(controllers["wikipediaUrl"],
            label: "Enter a Wikipedia URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["spotifyUrl"],
            label: "Enter a Spotify URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["youtubeUrl"],
            label: "Enter a Youtube URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["soundcloudUrl"],
            label: "Enter a Soundcloud URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["musicKey"],
            label: "Enter a music key",
            errorMessage: "Please enter a valid music key"),
        verticalSpacer(),
        optionalField(controllers["tuning"],
            label: "Enter a tuning",
            errorMessage: "Please enter a valid tuning"),
        verticalSpacer(),
        optionalField(controllers["musicCreationDate"],
            label: "Enter a music creation date",
            errorMessage: "Please enter a valid date"),
        verticalSpacer(),
        optionalField(controllers["albumName"],
            label: "Enter an album name",
            errorMessage: "Please enter a valid name"),
        verticalSpacer(),
        optionalField(controllers["albumInfo"],
            label: "Enter an album information URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["description"],
            label: "Enter a description",
            errorMessage: "Please enter a valid description",
            minLines: 2),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = InsertSongsRequest(songs: [
        InsertSongsRequestItem(
            displayName: fromTextController(controllers["displayName"]) ?? "",
            musicKey: fromTextController(controllers["musicKey"]),
            musicTuning: fromTextController(controllers["tuning"]),
            musicCreationDate:
                fromTextController(controllers["musicCreationDate"]),
            albumName: fromTextController(controllers["albumName"]),
            albumInfoLink: fromTextController(controllers["albumInfo"]),
            spotifyUrl: fromTextController(controllers["spotifyUrl"]),
            youtubeUrl: fromTextController(controllers["youtubeUrl"]),
            soundcloudUrl: fromTextController(controllers["soundcloudUrl"]),
            wikipediaUrl: fromTextController(controllers["wikipediaUrl"]),
            description: fromTextController(controllers["description"]))
      ]);
      await doHttp(widget.env, "${widget.env.apiUrl}/songs",
          method: "POST", data: r.toJson(), authToken: widget.env.authToken);

      // ignore: use_build_context_synchronously
      context.go("/songs");
    }

    return MyPage(
        env: widget.env,
        context: context,
        hasBackButton: true,
        backButtonDestination: "/management",
        routeName: "management",
        title: t(context).routeNames.newSong,
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Column(
            children: [
              header1(context, t(context).routeNames.newSong),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          )
        ]));
  }
}
