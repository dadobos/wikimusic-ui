import 'package:flutter/material.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/users.dart';

class DeleteUsersPage extends StatefulWidget {
  const DeleteUsersPage({super.key, required this.env});

  final Env env;

  @override
  State<DeleteUsersPage> createState() => DeleteUsersPageState();
}

class DeleteUsersPageState extends State<DeleteUsersPage> {
  final controllers = Map.fromEntries([
    MapEntry("email", TextEditingController()),
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Delete a user from this community!"),
        verticalSpacer(),
        requiredField(controllers["email"],
            validator: emailValidator,
            label: "Enter the email of the user",
            errorMessage: "Please enter a valid email"),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = DeleteUsersRequest(
        email: fromTextController(controllers["email"]) ?? "",
      );
      await doHttp(widget.env, "${widget.env.apiUrl}/users/delete",
          method: "POST", data: r.toJson(), authToken: widget.env.authToken);
    }

    return MyPage(
        env: widget.env,
        context: context,
        hasBackButton: true,
        backButtonDestination: "/management",
        title: "Delete a user",
        routeName: "management",
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Column(
            children: [
              header1(context, "Delete user"),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          ),
        ]));
  }
}
