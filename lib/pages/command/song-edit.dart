import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/songs.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongEditPage extends StatefulWidget {
  const SongEditPage(
      {super.key, required this.env, required this.songIdentifier});

  final Env env;
  final String songIdentifier;

  @override
  State<SongEditPage> createState() => SongEditPageState();
}

class SongEditPageState extends State<SongEditPage> {
  Future<Song>? song;

  @override
  void initState() {
    super.initState();
    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      song = fetchSongByIdentifier(widget.env, widget.songIdentifier);
    });
  }

  Widget editFormBuilder(
      final BuildContext context, final AsyncSnapshot<Song> snapshot) {
    if (snapshot.hasData && song != null) {
      final controllers = Map.fromEntries([
        MapEntry("displayName",
            TextEditingController(text: snapshot.data!.displayName)),
        MapEntry("spotifyUrl",
            TextEditingController(text: snapshot.data!.spotifyUrl)),
        MapEntry("soundcloudUrl",
            TextEditingController(text: snapshot.data!.soundcloudUrl)),
        MapEntry("wikipediaUrl",
            TextEditingController(text: snapshot.data!.wikipediaUrl)),
        MapEntry("youtubeUrl",
            TextEditingController(text: snapshot.data!.youtubeUrl)),
        MapEntry(
            "musicKey", TextEditingController(text: snapshot.data!.musicKey)),
        MapEntry("musicTuning",
            TextEditingController(text: snapshot.data!.musicTuning)),
        MapEntry("musicCreationDate",
            TextEditingController(text: snapshot.data!.musicCreationDate)),
        MapEntry(
            "albumName", TextEditingController(text: snapshot.data!.albumName)),
        MapEntry("albumInfo",
            TextEditingController(text: snapshot.data!.albumInfoLink)),
        MapEntry("description",
            TextEditingController(text: snapshot.data!.description))
      ]);
      final artworkControllers = Map.fromEntries([
        MapEntry("orderValue", TextEditingController()),
        MapEntry("contentUrl", TextEditingController()),
        MapEntry("contentCaption", TextEditingController()),
      ]);
      final artistControllers = Map.fromEntries(
          [MapEntry("artistIdentifier", TextEditingController())]);
      final w = padding(SizedBox(
        width: MediaQuery.of(context).size.width * 0.5,
        child: Column(
          children: [
            verticalSpacer(),
            header1(context, "Editing ${snapshot.data!.displayName}"),
            verticalSpacer(),
            const Text("Contribute to the WikiMusic knowledge base!"),
            verticalSpacer(),
            requiredField(controllers["displayName"],
                label: "Enter the name of the song",
                errorMessage: "Please enter a valid name"),
            verticalSpacer(),
            optionalField(controllers["wikipediaUrl"],
                label: "Enter a Wikipedia URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["spotifyUrl"],
                label: "Enter a Spotify URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["youtubeUrl"],
                label: "Enter a Youtube URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["soundcloudUrl"],
                label: "Enter a Soundcloud URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["musicKey"],
                label: "Enter a music key",
                errorMessage: "Please enter a valid music key"),
            verticalSpacer(),
            optionalField(controllers["musicTuning"],
                label: "Enter a music tuning",
                errorMessage: "Please enter a valid music tuning"),
            verticalSpacer(),
            optionalField(controllers["musicCreationDate"],
                label: "Enter a music creation date",
                errorMessage: "Please enter a valid date"),
            verticalSpacer(),
            optionalField(controllers["albumName"],
                label: "Enter an album name",
                errorMessage: "Please enter a valid name"),
            verticalSpacer(),
            optionalField(controllers["albumInfo"],
                label: "Enter an album info URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["description"],
                label: "Enter a description",
                errorMessage: "Please enter a valid description"),
          ],
        ),
      ));

      final List<SongArtwork> sortedArtworks =
          snapshot.data!.artworks.values.toList();
      sortedArtworks.sort((final a, final b) =>
          a.artwork.orderValue.compareTo(b.artwork.orderValue));
      final arts = sortedArtworks
          .map(
            (final e) => Column(
              children: [
                makeCardImage(context, e.artwork.contentUrl),
                if (e.artwork.contentCaption != null)
                  Text(e.artwork.contentCaption!),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    deleteButton(
                      context,
                      "Delete song artwork?",
                      () async {
                        await doHttp(widget.env,
                            "${widget.env.apiUrl}/songs/artworks/${e.artwork.identifier}",
                            method: "DELETE", authToken: widget.env.authToken);
                        refetchEntity();
                      },
                    ),
                    arrowUpButton(() async {
                      await doHttp(widget.env,
                          "${widget.env.apiUrl}/songs/artworks/order",
                          method: "PATCH",
                          authToken: widget.env.authToken,
                          data:
                              UpdateSongArtworkOrderRequest(songArtworkOrders: [
                            UpdateSongArtworkOrderRequestItem(
                                identifier: e.artwork.identifier,
                                orderValue: e.artwork.orderValue < 1
                                    ? 0
                                    : e.artwork.orderValue - 1)
                          ]).toJson());
                      refetchEntity();
                    }),
                    arrowDownButton(() async {
                      await doHttp(widget.env,
                          "${widget.env.apiUrl}/songs/artworks/order",
                          method: "PATCH",
                          authToken: widget.env.authToken,
                          data:
                              UpdateSongArtworkOrderRequest(songArtworkOrders: [
                            UpdateSongArtworkOrderRequestItem(
                                identifier: e.artwork.identifier,
                                orderValue: e.artwork.orderValue + 1)
                          ]).toJson());
                      refetchEntity();
                    })
                  ],
                )
              ],
            ),
          )
          .toList();

      final artworkFormWidget = padding(SizedBox(
        width: MediaQuery.of(context).size.width * 0.3,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            header2(context, "Artwork"),
            verticalSpacer(),
            ...arts,
            verticalSpacer(),
            Column(children: [
              requiredField(artworkControllers["contentUrl"],
                  label: "content URL"),
              verticalSpacer(),
              optionalField(artworkControllers["contentCaption"],
                  label: "content caption"),
              verticalSpacer(),
              requiredField(artworkControllers["orderValue"],
                  label: "order value"),
            ]),
          ],
        ),
      ));

      final artistFormWidget = padding(SizedBox(
        width: MediaQuery.of(context).size.width * 0.3,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(children: [
              header2(context, "Artist relation"),
              Column(
                  children: snapshot.data!.artists.entries.map(
                (final a) {
                  return Row(children: [
                    header3(context, a.value),
                    TextButton(
                        child: const Icon(Icons.delete, size: 16),
                        onPressed: () async {
                          await doHttp(
                              widget.env, "${widget.env.apiUrl}/songs/artists",
                              method: "DELETE",
                              authToken: widget.env.authToken,
                              data: SongArtistsRequest(songArtists: [
                                SongArtistsRequestItem(
                                    songIdentifier: snapshot.data!.identifier,
                                    artistIdentifier: a.key)
                              ]).toJson());
                          refetchEntity();
                        })
                  ]);
                },
              ).toList()),
              verticalSpacer(),
              requiredField(artistControllers["artistIdentifier"],
                  label: "artist identifier"),
            ]),
          ],
        ),
      ));

      Future<Null> onSubmit() async {
        final r = SongDeltaRequest(songDeltas: [
          SongDelta(
              identifier: snapshot.data!.identifier,
              displayName: fromTextController(controllers["displayName"]) ?? "",
              spotifyUrl: fromTextController(controllers["spotifyUrl"]),
              youtubeUrl: fromTextController(controllers["youtubeUrl"]),
              soundcloudUrl: fromTextController(controllers["soundcloudUrl"]),
              wikipediaUrl: fromTextController(controllers["wikipediaUrl"]),
              musicKey: fromTextController(controllers["musicKey"]),
              musicTuning: fromTextController(controllers["musicTuning"]),
              musicCreationDate:
                  fromTextController(controllers["musicCreationDate"]),
              albumName: fromTextController(controllers["albumName"]),
              albumInfoLink: fromTextController(controllers["albumInfo"]),
              description: fromTextController(controllers["description"]))
        ]);
        await doHttp(widget.env, "${widget.env.apiUrl}/songs/edit",
            method: "PATCH", data: r.toJson(), authToken: widget.env.authToken);

        // ignore: use_build_context_synchronously
        context.go("/songs/${snapshot.data!.identifier}");
      }

      Future<Null> onSubmitArtwork() async {
        final x = InsertSongArtworksRequest(songArtworks: [
          InsertSongArtworksRequestItem(
              songIdentifier: snapshot.data!.identifier,
              contentUrl:
                  fromTextController(artworkControllers["contentUrl"]) ?? "",
              contentCaption:
                  fromTextController(artworkControllers["contentCaption"]),
              orderValue: int.parse(
                  fromTextController(artworkControllers["orderValue"]) ?? "0"))
        ]);

        await doHttp(widget.env, "${widget.env.apiUrl}/songs/artworks",
            method: "POST", data: x.toJson(), authToken: widget.env.authToken);
        refetchEntity();
      }

      Future<Null> onSubmitArtist() async {
        final x = SongArtistsRequest(songArtists: [
          SongArtistsRequestItem(
              songIdentifier: snapshot.data!.identifier,
              artistIdentifier:
                  fromTextController(artistControllers["artistIdentifier"]) ??
                      "")
        ]);

        await doHttp(widget.env, "${widget.env.apiUrl}/songs/artists",
            method: "POST", data: x.toJson(), authToken: widget.env.authToken);
        refetchEntity();
      }

      return wrap(children: [
        ValidatedForm(
            env: widget.env,
            widget: w,
            onSubmit: onSubmit,
            controllers: controllers),
        ValidatedForm(
            env: widget.env,
            widget: artworkFormWidget,
            onSubmit: onSubmitArtwork,
            controllers: artworkControllers),
        ValidatedForm(
            env: widget.env,
            widget: artistFormWidget,
            onSubmit: onSubmitArtist,
            controllers: artistControllers)
      ]);
    } else if (snapshot.hasError) {
      return Text('${snapshot.error}');
    }
    return const CircularProgressIndicator();
  }

  @override
  Widget build(final BuildContext context) => MyPage(
      env: widget.env,
      context: context,
      title: t(context).routeNames.editSong,
      isLoggedIn: widget.env.authToken != null,
      hasBackButton: true,
      routeName: "management",
      backButtonDestination: "/songs/${widget.songIdentifier}",
      child: scrollableArea([
        Column(
          children: [
            verticalSpacer(),
            FutureBuilder(future: song, builder: editFormBuilder),
          ],
        ),
      ]));
}
