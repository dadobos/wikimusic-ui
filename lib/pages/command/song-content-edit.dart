import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/songs.dart';
import 'package:wikimusic_ui/model/songs.dart';

class SongContentEditPage extends StatefulWidget {
  const SongContentEditPage(
      {super.key,
      required this.env,
      required this.contentIdentifier,
      required this.songIdentifier});

  final Env env;
  final String contentIdentifier, songIdentifier;

  @override
  State<SongContentEditPage> createState() => SongContentEditPageState();
}

class SongContentEditPageState extends State<SongContentEditPage> {
  Future<Song>? song;

  @override
  void initState() {
    super.initState();
    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      song = fetchSongByIdentifier(widget.env, widget.songIdentifier);
    });
  }

  Widget editFormBuilder(
      final BuildContext context, final AsyncSnapshot<Song> snapshot) {
    if (snapshot.hasData && song != null) {
      final c = snapshot.data!.contents[widget.contentIdentifier]!;
      final controllers = Map.fromEntries([
        MapEntry("versionName", TextEditingController(text: c.versionName)),
        MapEntry(
            "instrumentType", TextEditingController(text: c.instrumentType)),
        MapEntry("asciiLegend", TextEditingController(text: c.asciiLegend)),
        MapEntry("asciiContents", TextEditingController(text: c.asciiContents)),
        MapEntry("pdfContents", TextEditingController(text: c.pdfContents)),
        MapEntry("guitarProContents",
            TextEditingController(text: c.guitarProContents))
      ]);

      final w = padding(Column(
        children: [
          verticalSpacer(),
          const Text("Contribute to the WikiMusic knowledge base!"),
          verticalSpacer(),
          requiredField(controllers["versionName"],
              label: "Enter the name of the version",
              errorMessage: "Please enter a valid name"),
          verticalSpacer(),
          optionalField(controllers["instrumentType"],
              label: "Enter an instrument name",
              errorMessage: "Please enter a valid instrument name"),
          verticalSpacer(),
          optionalField(controllers["asciiLegend"],
              label: "Enter ASCII legend",
              errorMessage: "Please enter a valid ASCII legend",
              keyboardType: TextInputType.multiline,
              minLines: 6,
              style: GoogleFonts.robotoMono(
                  textStyle: const TextStyle(
                fontWeight: FontWeight.w500,
              ))),
          verticalSpacer(),
          optionalField(controllers["asciiContents"],
              label: "Enter ASCII contents",
              errorMessage: "Please enter a valid ASCII contents",
              keyboardType: TextInputType.multiline,
              minLines: 12,
              style: GoogleFonts.robotoMono(
                  textStyle: const TextStyle(
                fontWeight: FontWeight.w500,
              ))),
          /* verticalSpacer(),
        optionalField(controllers["pdfContents"],
            label: "Enter a valid PDF file",
            errorMessage: "Please enter a valid PDF file"), */
          /* verticalSpacer(),
        optionalField(controllers["guitarProContents"],
            label: "Enter a Soundcloud URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(), */
        ],
      ));
      Future<Null> onSubmit() async {
        final r = SongContentDeltaRequest(songContentDeltas: [
          SongContentDelta(
              identifier: widget.contentIdentifier,
              versionName: fromTextController(controllers["versionName"]) ?? "",
              instrumentType: fromTextController(controllers["instrumentType"]),
              asciiLegend: fromTextController(controllers["asciiLegend"]),
              asciiContents: fromTextController(controllers["asciiContents"]),
              pdfContents: fromTextController(controllers["pdfContents"]),
              guitarProContents:
                  fromTextController(controllers["guitarProContents"]))
        ]);
        await doHttp(widget.env, "${widget.env.apiUrl}/songs/contents",
            method: "PATCH", data: r.toJson(), authToken: widget.env.authToken);

        // ignore: use_build_context_synchronously
        context.go("/songs/${widget.songIdentifier}");
      }

      return ValidatedForm(
          env: widget.env,
          widget: widget,
          onSubmit: onSubmit,
          controllers: controllers);
    } else if (snapshot.hasError) {
      return Text('${snapshot.error}');
    }
    return const CircularProgressIndicator();
  }

  @override
  Widget build(final BuildContext context) {
    return MyPage(
        env: widget.env,
        context: context,
        routeName: "management",
        title: t(context).routeNames.editSongContent,
        isLoggedIn: widget.env.authToken != null,
        hasBackButton: true,
        backButtonDestination: "/songs/${widget.songIdentifier}",
        child: scrollableArea([
          Column(
            children: [
              verticalSpacer(),
              FutureBuilder(future: song, builder: editFormBuilder),
            ],
          ),
        ]));
  }
}
