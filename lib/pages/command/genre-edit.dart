import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/http/genres.dart';
import 'package:wikimusic_ui/model/genres.dart';

class GenreEditPage extends StatefulWidget {
  const GenreEditPage(
      {super.key, required this.env, required this.genreIdentifier});

  final Env env;
  final String genreIdentifier;

  @override
  State<GenreEditPage> createState() => GenreEditPageState();
}

class GenreEditPageState extends State<GenreEditPage> {
  Future<Genre>? genre;

  @override
  void initState() {
    super.initState();
    refetchEntity();
  }

  void refetchEntity() {
    setState(() {
      genre = fetchGenreByIdentifier(widget.env, widget.genreIdentifier);
    });
  }

  Widget editFormBuilder(
      final BuildContext context, final AsyncSnapshot<Genre> snapshot) {
    if (snapshot.hasData && genre != null) {
      final controllers = Map.fromEntries([
        MapEntry("displayName",
            TextEditingController(text: snapshot.data!.displayName)),
        MapEntry("spotifyUrl",
            TextEditingController(text: snapshot.data!.spotifyUrl)),
        MapEntry("soundcloudUrl",
            TextEditingController(text: snapshot.data!.soundcloudUrl)),
        MapEntry("wikipediaUrl",
            TextEditingController(text: snapshot.data!.wikipediaUrl)),
        MapEntry("youtubeUrl",
            TextEditingController(text: snapshot.data!.youtubeUrl)),
        MapEntry("description",
            TextEditingController(text: snapshot.data!.description))
      ]);
      final w = padding(SizedBox(
        width: MediaQuery.of(context).size.width * 0.5,
        child: Column(
          children: [
            verticalSpacer(),
            header1(context, "Editing ${snapshot.data!.displayName}"),
            verticalSpacer(),
            const Text("Contribute to the WikiMusic knowledge base!"),
            verticalSpacer(),
            requiredField(controllers["displayName"],
                label: "Enter the name of the genre",
                errorMessage: "Please enter a valid name"),
            verticalSpacer(),
            optionalField(controllers["wikipediaUrl"],
                label: "Enter a Wikipedia URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["spotifyUrl"],
                label: "Enter a Spotify URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["youtubeUrl"],
                label: "Enter a Youtube URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["soundcloudUrl"],
                label: "Enter a Soundcloud URL",
                errorMessage: "Please enter a valid URL"),
            verticalSpacer(),
            optionalField(controllers["description"],
                label: "Enter a description",
                errorMessage: "Please enter a valid description",
                minLines: 2),
          ],
        ),
      ));

      final List<GenreArtwork> sortedArtworks =
          snapshot.data!.artworks.values.toList();
      sortedArtworks.sort((final a, final b) =>
          a.artwork.orderValue.compareTo(b.artwork.orderValue));
      final arts = sortedArtworks
          .map(
            (final e) => Column(
              children: [
                makeCardImage(context, e.artwork.contentUrl),
                if (e.artwork.contentCaption != null)
                  Text(e.artwork.contentCaption!),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    deleteButton(
                      context,
                      "Delete genre artwork?",
                      () async {
                        await doHttp(widget.env,
                            "${widget.env.apiUrl}/genres/artworks/${e.artwork.identifier}",
                            method: "DELETE", authToken: widget.env.authToken);
                        refetchEntity();
                      },
                    ),
                    arrowUpButton(() async {
                      await doHttp(widget.env,
                          "${widget.env.apiUrl}/genres/artworks/order",
                          method: "PATCH",
                          authToken: widget.env.authToken,
                          data: UpdateGenreArtworkOrderRequest(
                              genreArtworkOrders: [
                                UpdateGenreArtworkOrderRequestItem(
                                    identifier: e.artwork.identifier,
                                    orderValue: e.artwork.orderValue < 1
                                        ? 0
                                        : e.artwork.orderValue - 1)
                              ]).toJson());
                      refetchEntity();
                    }),
                    arrowDownButton(() async {
                      await doHttp(widget.env,
                          "${widget.env.apiUrl}/genres/artworks/order",
                          method: "PATCH",
                          authToken: widget.env.authToken,
                          data: UpdateGenreArtworkOrderRequest(
                              genreArtworkOrders: [
                                UpdateGenreArtworkOrderRequestItem(
                                    identifier: e.artwork.identifier,
                                    orderValue: e.artwork.orderValue + 1)
                              ]).toJson());
                      refetchEntity();
                    })
                  ],
                )
              ],
            ),
          )
          .toList();

      final artworkControllers = Map.fromEntries([
        MapEntry("orderValue", TextEditingController()),
        MapEntry("contentUrl", TextEditingController()),
        MapEntry("contentCaption", TextEditingController()),
      ]);

      final artworkFormWidget = padding(SizedBox(
        width: MediaQuery.of(context).size.width * 0.3,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            header2(context, "Artwork"),
            verticalSpacer(),
            ...arts,
            verticalSpacer(),
            Column(children: [
              requiredField(artworkControllers["contentUrl"],
                  label: "content URL"),
              verticalSpacer(),
              optionalField(artworkControllers["contentCaption"],
                  label: "content caption"),
              verticalSpacer(),
              requiredField(artworkControllers["orderValue"],
                  label: "order value"),
            ]),
          ],
        ),
      ));

      Future<Null> onSubmit() async {
        final r = GenreDeltaRequest(genreDeltas: [
          GenreDelta(
              identifier: snapshot.data!.identifier,
              displayName: fromTextController(controllers["displayName"]) ?? "",
              spotifyUrl: fromTextController(controllers["spotifyUrl"]),
              youtubeUrl: fromTextController(controllers["youtubeUrl"]),
              soundcloudUrl: fromTextController(controllers["soundcloudUrl"]),
              wikipediaUrl: fromTextController(controllers["wikipediaUrl"]),
              description: fromTextController(controllers["description"]))
        ]);
        await doHttp(widget.env, "${widget.env.apiUrl}/genres/edit",
            method: "PATCH", data: r.toJson(), authToken: widget.env.authToken);

        // ignore: use_build_context_synchronously
        context.go("/genres/${snapshot.data!.identifier}");
      }

      Future<Null> onSubmitArtwork() async {
        final x = InsertGenreArtworksRequest(genreArtworks: [
          InsertGenreArtworksRequestItem(
              genreIdentifier: snapshot.data!.identifier,
              contentUrl:
                  fromTextController(artworkControllers["contentUrl"]) ?? "",
              contentCaption:
                  fromTextController(artworkControllers["contentCaption"]),
              orderValue: int.parse(
                  fromTextController(artworkControllers["orderValue"]) ?? "0"))
        ]);

        await doHttp(widget.env, "${widget.env.apiUrl}/genres/artworks",
            method: "POST", data: x.toJson(), authToken: widget.env.authToken);
        refetchEntity();
      }

      return wrap(children: [
        ValidatedForm(
            env: widget.env,
            widget: w,
            onSubmit: onSubmit,
            controllers: controllers),
        ValidatedForm(
            env: widget.env,
            widget: artworkFormWidget,
            onSubmit: onSubmitArtwork,
            controllers: artworkControllers)
      ]);
    } else if (snapshot.hasError) {
      return Text('${snapshot.error}');
    }
    return const CircularProgressIndicator();
  }

  @override
  Widget build(final BuildContext context) => MyPage(
      env: widget.env,
      context: context,
      title: t(context).routeNames.editGenre,
      routeName: "management",
      isLoggedIn: widget.env.authToken != null,
      hasBackButton: true,
      backButtonDestination: "/genres/${widget.genreIdentifier}",
      child: scrollableArea([
        Column(
          children: [
            verticalSpacer(),
            FutureBuilder(future: genre, builder: editFormBuilder),
          ],
        ),
      ]));
}
