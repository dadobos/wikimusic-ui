import 'package:flutter/material.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/users.dart';

class InviteUsersPage extends StatefulWidget {
  const InviteUsersPage({super.key, required this.env});

  final Env env;

  @override
  State<InviteUsersPage> createState() => InviteUsersPageState();
}

class InviteUsersPageState extends State<InviteUsersPage> {
  final controllers = Map.fromEntries([
    MapEntry("displayName", TextEditingController()),
    MapEntry("email", TextEditingController()),
    MapEntry("role", TextEditingController()),
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Invite a user to this community!"),
        verticalSpacer(),
        requiredField(controllers["displayName"],
            label: "Enter the name of the user",
            errorMessage: "Please enter a valid name"),
        verticalSpacer(),
        requiredField(controllers["email"],
            validator: emailValidator,
            label: "Enter an email",
            errorMessage: "Please enter a valid email"),
        verticalSpacer(),
        DropdownMenu(
          inputDecorationTheme: InputDecorationTheme(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16))),
          controller: controllers["role"],
          initialSelection: "wm::demo",
          dropdownMenuEntries: const [
            DropdownMenuEntry(value: "wm::demo", label: "wm::demo"),
            DropdownMenuEntry(value: "wm::lowrank", label: "wm::lowrank"),
            DropdownMenuEntry(value: "wm::maintainer", label: "wm::maintainer"),
            DropdownMenuEntry(value: "wm::superuser", label: "wm::superuser")
          ],
        ),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = InviteUsersRequest(
        displayName: fromTextController(controllers["displayName"]) ?? "",
        email: fromTextController(controllers["email"]) ?? "",
        role: fromTextController(controllers["role"]) ?? "",
      );
      await doHttp(widget.env, "${widget.env.apiUrl}/users/invite",
          method: "POST", data: r.toJson(), authToken: widget.env.authToken);
    }

    return MyPage(
        env: widget.env,
        context: context,
        title: "Invite a user",
        routeName: "management",
        hasBackButton: true,
        backButtonDestination: "/management",
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Column(
            children: [
              header1(context, "Invite user"),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          ),
        ]));
  }
}
