import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/page.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/form.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/artists.dart';

class ArtistAddPage extends StatefulWidget {
  const ArtistAddPage({super.key, required this.env});

  final Env env;

  @override
  State<ArtistAddPage> createState() => ArtistAddPageState();
}

class ArtistAddPageState extends State<ArtistAddPage> {
  final controllers = Map.fromEntries([
    MapEntry("displayName", TextEditingController()),
    MapEntry("wikipediaUrl", TextEditingController()),
    MapEntry("spotifyUrl", TextEditingController()),
    MapEntry("youtubeUrl", TextEditingController()),
    MapEntry("soundcloudUrl", TextEditingController()),
    MapEntry("description", TextEditingController())
  ]);

  @override
  Widget build(final BuildContext context) {
    final formWidget = padding(Column(
      children: [
        verticalSpacer(),
        const Text("Contribute to the WikiMusic knowledge base!"),
        verticalSpacer(),
        requiredField(controllers["displayName"],
            label: "Enter the name of the genre",
            errorMessage: "Please enter a valid name"),
        verticalSpacer(),
        optionalField(controllers["wikipediaUrl"],
            label: "Enter a Wikipedia URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["spotifyUrl"]!,
            label: "Enter a Spotify URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["youtubeUrl"]!,
            label: "Enter a Youtube URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["soundcloudUrl"]!,
            label: "Enter a Soundcloud URL",
            errorMessage: "Please enter a valid URL"),
        verticalSpacer(),
        optionalField(controllers["description"]!,
            label: "Enter a description",
            errorMessage: "Please enter a valid description",
            minLines: 2),
      ],
    ));

    Future<Null> onSubmit() async {
      final r = InsertArtistsRequest(artists: [
        InsertArtistsRequestItem(
          displayName: fromTextController(controllers["displayName"]) ?? "",
          spotifyUrl: fromTextController(controllers["spotifyUrl"]),
          youtubeUrl: fromTextController(controllers["youtubeUrl"]),
          soundcloudUrl: fromTextController(controllers["soundcloudUrl"]),
          wikipediaUrl: fromTextController(controllers["wikipediaUrl"]),
          description: fromTextController(controllers["description"]),
        )
      ]);
      await doHttp(widget.env, "${widget.env.apiUrl}/artists",
          method: "POST", data: r.toJson(), authToken: widget.env.authToken);

      // ignore: use_build_context_synchronously
      context.go("/artists");
    }

    return MyPage(
        env: widget.env,
        context: context,
        hasBackButton: true,
        routeName: "management",
        backButtonDestination: "/management",
        title: t(context).routeNames.newArtist,
        isLoggedIn: widget.env.authToken != null,
        child: scrollableArea([
          Column(
            children: [
              header1(context, t(context).routeNames.newArtist),
              verticalSpacer(),
              ValidatedForm(
                  env: widget.env,
                  widget: formWidget,
                  onSubmit: onSubmit,
                  controllers: controllers)
            ],
          ),
        ]));
  }
}
