import 'package:dio/dio.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:time_machine/time_machine.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/l10n/l10n.dart';
import 'package:wikimusic_ui/theme/theme.dart';

Future<Env> buildEnv() async {
  setupLogging();

  const String apiUrl = String.fromEnvironment("API_URL",
      defaultValue: "https://api.wikimusic.jointhefreeworld.org");

  final dio = Dio();
  final now = ZonedDateTime(Instant.now());
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return Env(
      dio: dio,
      apiUrl: apiUrl,
      authToken: prefs.getString("authToken"),
      prefersDark: prefs.getBool("prefersDark") ?? false,
      lastEmailUsed: prefs.getString("lastEmailUsed") ?? "",
      locale: prefs.getString("locale") ?? "en",
      lastLoggedInAt: now);
}

void setupLogging() {
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((final record) {
    // ignore: avoid_print
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
}

DynamicColorBuilder withTheme(
        final ValueNotifier<ThemeSettings> themeSettingsNotifier,
        final Widget Function(BuildContext, ThemeSettings, Widget?) builder) =>
    DynamicColorBuilder(
      builder:
          (final ColorScheme? lightDynamic, final ColorScheme? darkDynamic) =>
              ThemeProvider(
                  lightDynamic: lightDynamic,
                  darkDynamic: darkDynamic,
                  settings: themeSettingsNotifier,
                  child: NotificationListener<ThemeSettingChange>(
                    onNotification: (final ThemeSettingChange notification) {
                      themeSettingsNotifier.value = notification.settings;
                      return true;
                    },
                    child: ValueListenableBuilder<ThemeSettings>(
                      valueListenable: themeSettingsNotifier,
                      builder: builder,
                    ),
                  )),
    );

TranslationProvider withTranslations(
        final ValueNotifier<LanguageSettings> languageSettingsNotifier,
        final Widget Function(BuildContext, LanguageSettings, Widget?)
            builder) =>
    TranslationProvider(
        languageSettings: languageSettingsNotifier,
        child: NotificationListener<LanguageChanged>(
          onNotification: (final LanguageChanged notification) {
            languageSettingsNotifier.value = notification.languageSettings;
            return true;
          },
          child: ValueListenableBuilder<LanguageSettings>(
              valueListenable: languageSettingsNotifier, builder: builder),
        ));

UserPreferencesProvider withUserPreferences(
        final ValueNotifier<UserPreferences> userPreferencesNotifier,
        final Widget Function(BuildContext, UserPreferences, Widget?)
            builder) =>
    UserPreferencesProvider(
        userPreferences: userPreferencesNotifier,
        child: NotificationListener<UserPreferencesChanged>(
            onNotification: (final UserPreferencesChanged notification) {
              userPreferencesNotifier.value = notification.userPreferences;
              return true;
            },
            child: ValueListenableBuilder<UserPreferences>(
                valueListenable: userPreferencesNotifier, builder: builder)));
