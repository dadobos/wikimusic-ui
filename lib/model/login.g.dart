// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginRequest _$LoginRequestFromJson(final Map<String, dynamic> json) =>
    LoginRequest(
      wikimusicEmail: json['wikimusicEmail'] as String,
      wikimusicPassword: json['wikimusicPassword'] as String,
    );

Map<String, dynamic> _$LoginRequestToJson(final LoginRequest instance) =>
    <String, dynamic>{
      'wikimusicEmail': instance.wikimusicEmail,
      'wikimusicPassword': instance.wikimusicPassword,
    };

GetMeResponse _$GetMeResponseFromJson(final Map<String, dynamic> json) =>
    GetMeResponse(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      emailAddress: json['emailAddress'] as String,
      roles: (json['roles'] as List<dynamic>)
          .map((final e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$GetMeResponseToJson(final GetMeResponse instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'emailAddress': instance.emailAddress,
      'roles': instance.roles,
    };
