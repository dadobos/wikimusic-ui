import 'package:json_annotation/json_annotation.dart';

part 'other.g.dart';

@JsonSerializable()
class Opinion {
  final String identifier, createdBy;
  final bool isLike;
  final bool isDislike;
  final DateTime createdAt;
  final DateTime? lastEditedAt;

  const Opinion(
      {required this.identifier,
      required this.createdBy,
      required this.isLike,
      required this.isDislike,
      required this.createdAt,
      this.lastEditedAt});

  factory Opinion.fromJson(final Map<String, dynamic> json) =>
      _$OpinionFromJson(json);
  Map<String, dynamic> toJson() => _$OpinionToJson(this);
}

@JsonSerializable()
class Artwork {
  final String identifier, createdBy;
  final int visibilityStatus;
  final String? approvedBy;
  final String contentUrl;
  final String? contentCaption;
  final DateTime createdAt;
  final DateTime? lastEditedAt;
  final int orderValue;

  const Artwork(
      {required this.identifier,
      required this.createdBy,
      required this.visibilityStatus,
      this.approvedBy,
      required this.contentUrl,
      this.contentCaption,
      required this.createdAt,
      this.lastEditedAt,
      required this.orderValue});

  factory Artwork.fromJson(final Map<String, dynamic> json) =>
      _$ArtworkFromJson(json);
  Map<String, dynamic> toJson() => _$ArtworkToJson(this);
}
