import 'package:json_annotation/json_annotation.dart';
import 'package:wikimusic_ui/model/other.dart';

part 'artists.g.dart';

@JsonSerializable()
class ArtistOpinion {
  final String artistIdentifier;
  final Opinion opinion;

  const ArtistOpinion({
    required this.opinion,
    required this.artistIdentifier,
  });

  factory ArtistOpinion.fromJson(final Map<String, dynamic> json) =>
      _$ArtistOpinionFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistOpinionToJson(this);
}

@JsonSerializable()
class ArtistArtwork {
  final String artistIdentifier;
  final Artwork artwork;

  const ArtistArtwork({
    required this.artwork,
    required this.artistIdentifier,
  });

  factory ArtistArtwork.fromJson(final Map<String, dynamic> json) =>
      _$ArtistArtworkFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistArtworkToJson(this);
}

@JsonSerializable()
class Artist {
  final String identifier;
  final String displayName;
  final String createdBy;
  final int visibilityStatus;
  final String? approvedBy;
  final DateTime createdAt;
  final DateTime? lastEditedAt;

  final Map<String, ArtistArtwork> artworks;
  final Map<String, ArtistOpinion> opinions;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final int viewCount;
  final String? description;

  const Artist(
      {required this.identifier,
      required this.displayName,
      required this.createdBy,
      required this.visibilityStatus,
      this.approvedBy,
      required this.createdAt,
      this.lastEditedAt,
      required this.artworks,
      required this.opinions,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      required this.viewCount,
      this.description});
  factory Artist.fromJson(final Map<String, dynamic> json) =>
      _$ArtistFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistToJson(this);
}

@JsonSerializable()
class GetArtistsQueryResponse {
  final Map<String, Artist> artists;
  final List<String> sortOrder;
  const GetArtistsQueryResponse(
      {required this.artists, required this.sortOrder});

  factory GetArtistsQueryResponse.fromJson(final Map<String, dynamic> json) =>
      _$GetArtistsQueryResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GetArtistsQueryResponseToJson(this);
}

@JsonSerializable()
class InsertArtistsRequestItem {
  final String displayName;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const InsertArtistsRequestItem(
      {required this.displayName,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.description});

  factory InsertArtistsRequestItem.fromJson(final Map<String, dynamic> json) =>
      _$InsertArtistsRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertArtistsRequestItemToJson(this);
}

@JsonSerializable()
class InsertArtistsRequest {
  final List<InsertArtistsRequestItem> artists;
  const InsertArtistsRequest({required this.artists});

  factory InsertArtistsRequest.fromJson(final Map<String, dynamic> json) =>
      _$InsertArtistsRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertArtistsRequestToJson(this);
}

@JsonSerializable()
class ArtistDeltaRequest {
  final List<ArtistDelta> artistDeltas;

  const ArtistDeltaRequest({required this.artistDeltas});

  factory ArtistDeltaRequest.fromJson(final Map<String, dynamic> json) =>
      _$ArtistDeltaRequestFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistDeltaRequestToJson(this);
}

@JsonSerializable()
class ArtistDelta {
  final String identifier;
  final String displayName;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const ArtistDelta(
      {required this.identifier,
      required this.displayName,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.description});

  factory ArtistDelta.fromJson(final Map<String, dynamic> json) =>
      _$ArtistDeltaFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistDeltaToJson(this);
}

@JsonSerializable()
class InsertArtistArtworksRequest {
  final List<InsertArtistArtworksRequestItem> artistArtworks;
  const InsertArtistArtworksRequest({required this.artistArtworks});

  factory InsertArtistArtworksRequest.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertArtistArtworksRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertArtistArtworksRequestToJson(this);
}

@JsonSerializable()
class InsertArtistArtworksRequestItem {
  final String artistIdentifier;
  final String contentUrl;
  final String? contentCaption;
  final int orderValue;

  const InsertArtistArtworksRequestItem(
      {required this.artistIdentifier,
      required this.contentUrl,
      this.contentCaption,
      required this.orderValue});

  factory InsertArtistArtworksRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertArtistArtworksRequestItemFromJson(json);
  Map<String, dynamic> toJson() =>
      _$InsertArtistArtworksRequestItemToJson(this);
}

@JsonSerializable()
class UpdateArtistArtworkOrderRequest {
  final List<UpdateArtistArtworkOrderRequestItem> artistArtworkOrders;
  const UpdateArtistArtworkOrderRequest({required this.artistArtworkOrders});
  factory UpdateArtistArtworkOrderRequest.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateArtistArtworkOrderRequestFromJson(json);
  Map<String, dynamic> toJson() =>
      _$UpdateArtistArtworkOrderRequestToJson(this);
}

@JsonSerializable()
class UpdateArtistArtworkOrderRequestItem {
  final String identifier;
  final int orderValue;
  const UpdateArtistArtworkOrderRequestItem(
      {required this.identifier, required this.orderValue});
  factory UpdateArtistArtworkOrderRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateArtistArtworkOrderRequestItemFromJson(json);
  Map<String, dynamic> toJson() =>
      _$UpdateArtistArtworkOrderRequestItemToJson(this);
}
