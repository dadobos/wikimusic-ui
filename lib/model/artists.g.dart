// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artists.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArtistOpinion _$ArtistOpinionFromJson(Map<String, dynamic> json) =>
    ArtistOpinion(
      opinion: Opinion.fromJson(json['opinion'] as Map<String, dynamic>),
      artistIdentifier: json['artistIdentifier'] as String,
    );

Map<String, dynamic> _$ArtistOpinionToJson(ArtistOpinion instance) =>
    <String, dynamic>{
      'artistIdentifier': instance.artistIdentifier,
      'opinion': instance.opinion,
    };

ArtistArtwork _$ArtistArtworkFromJson(Map<String, dynamic> json) =>
    ArtistArtwork(
      artwork: Artwork.fromJson(json['artwork'] as Map<String, dynamic>),
      artistIdentifier: json['artistIdentifier'] as String,
    );

Map<String, dynamic> _$ArtistArtworkToJson(ArtistArtwork instance) =>
    <String, dynamic>{
      'artistIdentifier': instance.artistIdentifier,
      'artwork': instance.artwork,
    };

Artist _$ArtistFromJson(Map<String, dynamic> json) => Artist(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      createdBy: json['createdBy'] as String,
      visibilityStatus: (json['visibilityStatus'] as num).toInt(),
      approvedBy: json['approvedBy'] as String?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
      artworks: (json['artworks'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, ArtistArtwork.fromJson(e as Map<String, dynamic>)),
      ),
      opinions: (json['opinions'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, ArtistOpinion.fromJson(e as Map<String, dynamic>)),
      ),
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      viewCount: (json['viewCount'] as num).toInt(),
      description: json['description'] as String?,
    );

Map<String, dynamic> _$ArtistToJson(Artist instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'createdBy': instance.createdBy,
      'visibilityStatus': instance.visibilityStatus,
      'approvedBy': instance.approvedBy,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
      'artworks': instance.artworks,
      'opinions': instance.opinions,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'viewCount': instance.viewCount,
      'description': instance.description,
    };

GetArtistsQueryResponse _$GetArtistsQueryResponseFromJson(
        Map<String, dynamic> json) =>
    GetArtistsQueryResponse(
      artists: (json['artists'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, Artist.fromJson(e as Map<String, dynamic>)),
      ),
      sortOrder:
          (json['sortOrder'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$GetArtistsQueryResponseToJson(
        GetArtistsQueryResponse instance) =>
    <String, dynamic>{
      'artists': instance.artists,
      'sortOrder': instance.sortOrder,
    };

InsertArtistsRequestItem _$InsertArtistsRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertArtistsRequestItem(
      displayName: json['displayName'] as String,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$InsertArtistsRequestItemToJson(
        InsertArtistsRequestItem instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertArtistsRequest _$InsertArtistsRequestFromJson(
        Map<String, dynamic> json) =>
    InsertArtistsRequest(
      artists: (json['artists'] as List<dynamic>)
          .map((e) =>
              InsertArtistsRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertArtistsRequestToJson(
        InsertArtistsRequest instance) =>
    <String, dynamic>{
      'artists': instance.artists,
    };

ArtistDeltaRequest _$ArtistDeltaRequestFromJson(Map<String, dynamic> json) =>
    ArtistDeltaRequest(
      artistDeltas: (json['artistDeltas'] as List<dynamic>)
          .map((e) => ArtistDelta.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ArtistDeltaRequestToJson(ArtistDeltaRequest instance) =>
    <String, dynamic>{
      'artistDeltas': instance.artistDeltas,
    };

ArtistDelta _$ArtistDeltaFromJson(Map<String, dynamic> json) => ArtistDelta(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$ArtistDeltaToJson(ArtistDelta instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertArtistArtworksRequest _$InsertArtistArtworksRequestFromJson(
        Map<String, dynamic> json) =>
    InsertArtistArtworksRequest(
      artistArtworks: (json['artistArtworks'] as List<dynamic>)
          .map((e) => InsertArtistArtworksRequestItem.fromJson(
              e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertArtistArtworksRequestToJson(
        InsertArtistArtworksRequest instance) =>
    <String, dynamic>{
      'artistArtworks': instance.artistArtworks,
    };

InsertArtistArtworksRequestItem _$InsertArtistArtworksRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertArtistArtworksRequestItem(
      artistIdentifier: json['artistIdentifier'] as String,
      contentUrl: json['contentUrl'] as String,
      contentCaption: json['contentCaption'] as String?,
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$InsertArtistArtworksRequestItemToJson(
        InsertArtistArtworksRequestItem instance) =>
    <String, dynamic>{
      'artistIdentifier': instance.artistIdentifier,
      'contentUrl': instance.contentUrl,
      'contentCaption': instance.contentCaption,
      'orderValue': instance.orderValue,
    };

UpdateArtistArtworkOrderRequest _$UpdateArtistArtworkOrderRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateArtistArtworkOrderRequest(
      artistArtworkOrders: (json['artistArtworkOrders'] as List<dynamic>)
          .map((e) => UpdateArtistArtworkOrderRequestItem.fromJson(
              e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UpdateArtistArtworkOrderRequestToJson(
        UpdateArtistArtworkOrderRequest instance) =>
    <String, dynamic>{
      'artistArtworkOrders': instance.artistArtworkOrders,
    };

UpdateArtistArtworkOrderRequestItem
    _$UpdateArtistArtworkOrderRequestItemFromJson(Map<String, dynamic> json) =>
        UpdateArtistArtworkOrderRequestItem(
          identifier: json['identifier'] as String,
          orderValue: (json['orderValue'] as num).toInt(),
        );

Map<String, dynamic> _$UpdateArtistArtworkOrderRequestItemToJson(
        UpdateArtistArtworkOrderRequestItem instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'orderValue': instance.orderValue,
    };
