import 'package:json_annotation/json_annotation.dart';
import 'package:wikimusic_ui/model/other.dart';

part 'songs.g.dart';

@JsonSerializable()
class SongOpinion {
  final String songIdentifier;
  final Opinion opinion;

  const SongOpinion({
    required this.opinion,
    required this.songIdentifier,
  });

  factory SongOpinion.fromJson(final Map<String, dynamic> json) =>
      _$SongOpinionFromJson(json);
  Map<String, dynamic> toJson() => _$SongOpinionToJson(this);
}

@JsonSerializable()
class SongContent {
  final String identifier, songIdentifier, createdBy;
  final String versionName;
  final int visibilityStatus;
  final String? approvedBy;
  final String instrumentType;
  final String? asciiLegend;
  final String? asciiContents;
  final String? pdfContents;
  final String? guitarProContents;
  final DateTime createdAt;
  final DateTime? lastEditedAt;

  const SongContent(
      {required this.identifier,
      required this.songIdentifier,
      required this.createdBy,
      required this.versionName,
      required this.visibilityStatus,
      this.approvedBy,
      required this.instrumentType,
      this.asciiLegend,
      this.asciiContents,
      this.pdfContents,
      this.guitarProContents,
      required this.createdAt,
      this.lastEditedAt});

  factory SongContent.fromJson(final Map<String, dynamic> json) =>
      _$SongContentFromJson(json);
  Map<String, dynamic> toJson() => _$SongContentToJson(this);
}

@JsonSerializable()
class SongArtwork {
  final String songIdentifier;
  final Artwork artwork;

  const SongArtwork({
    required this.artwork,
    required this.songIdentifier,
  });

  factory SongArtwork.fromJson(final Map<String, dynamic> json) =>
      _$SongArtworkFromJson(json);
  Map<String, dynamic> toJson() => _$SongArtworkToJson(this);
}

@JsonSerializable()
class Song {
  final String identifier;
  final String displayName;
  final String? musicKey;
  final String? musicTuning;
  final String? musicCreationDate;
  final String? albumName;
  final String? albumInfoLink;
  final String createdBy;
  final int visibilityStatus;
  final String? approvedBy;
  final DateTime createdAt;
  final DateTime? lastEditedAt;

  final Map<String, SongArtwork> artworks;
  final Map<String, String> artists;
  final Map<String, SongOpinion> opinions;
  final Map<String, SongContent> contents;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final int viewCount;
  final String? description;

  const Song(
      {required this.identifier,
      required this.displayName,
      this.musicKey,
      this.musicTuning,
      this.musicCreationDate,
      this.albumName,
      this.albumInfoLink,
      required this.createdBy,
      required this.visibilityStatus,
      this.approvedBy,
      required this.createdAt,
      this.lastEditedAt,
      required this.contents,
      required this.artists,
      required this.artworks,
      required this.opinions,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      required this.viewCount,
      this.description});
  factory Song.fromJson(final Map<String, dynamic> json) =>
      _$SongFromJson(json);
  Map<String, dynamic> toJson() => _$SongToJson(this);
}

@JsonSerializable()
class GetSongsQueryResponse {
  final Map<String, Song> songs;
  final List<String> sortOrder;
  const GetSongsQueryResponse({required this.songs, required this.sortOrder});

  factory GetSongsQueryResponse.fromJson(final Map<String, dynamic> json) =>
      _$GetSongsQueryResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GetSongsQueryResponseToJson(this);
}

@JsonSerializable()
class InsertSongsRequestItem {
  final String displayName;
  final String? musicKey;
  final String? musicTuning;
  final String? musicCreationDate;
  final String? albumName;
  final String? albumInfoLink;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const InsertSongsRequestItem(
      {required this.displayName,
      this.musicKey,
      this.musicTuning,
      this.musicCreationDate,
      this.albumName,
      this.albumInfoLink,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.description});

  factory InsertSongsRequestItem.fromJson(final Map<String, dynamic> json) =>
      _$InsertSongsRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongsRequestItemToJson(this);
}

@JsonSerializable()
class InsertSongsRequest {
  final List<InsertSongsRequestItem> songs;
  const InsertSongsRequest({required this.songs});

  factory InsertSongsRequest.fromJson(final Map<String, dynamic> json) =>
      _$InsertSongsRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongsRequestToJson(this);
}

@JsonSerializable()
class SongDeltaRequest {
  final List<SongDelta> songDeltas;
  const SongDeltaRequest({required this.songDeltas});

  factory SongDeltaRequest.fromJson(final Map<String, dynamic> json) =>
      _$SongDeltaRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SongDeltaRequestToJson(this);
}

@JsonSerializable()
class SongDelta {
  final String identifier;
  final String displayName;
  final String? musicKey;
  final String? musicTuning;
  final String? musicCreationDate;
  final String? albumName;
  final String? albumInfoLink;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const SongDelta(
      {required this.identifier,
      required this.displayName,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.musicKey,
      this.musicTuning,
      this.musicCreationDate,
      this.albumName,
      this.albumInfoLink,
      this.description});

  factory SongDelta.fromJson(final Map<String, dynamic> json) =>
      _$SongDeltaFromJson(json);
  Map<String, dynamic> toJson() => _$SongDeltaToJson(this);
}

@JsonSerializable()
class InsertSongArtworksRequest {
  final List<InsertSongArtworksRequestItem> songArtworks;
  const InsertSongArtworksRequest({required this.songArtworks});

  factory InsertSongArtworksRequest.fromJson(final Map<String, dynamic> json) =>
      _$InsertSongArtworksRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongArtworksRequestToJson(this);
}

@JsonSerializable()
class InsertSongArtworksRequestItem {
  final String songIdentifier;
  final String contentUrl;
  final String? contentCaption;
  final int orderValue;

  const InsertSongArtworksRequestItem(
      {required this.songIdentifier,
      required this.contentUrl,
      this.contentCaption,
      required this.orderValue});

  factory InsertSongArtworksRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertSongArtworksRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongArtworksRequestItemToJson(this);
}

@JsonSerializable()
class InsertSongContentsRequestItem {
  final String songIdentifier;
  final String versionName;
  final String? instrumentType;
  final String? asciiLegend;
  final String? asciiContents;
  final String? pdfContents;
  final String? guitarProContents;

  const InsertSongContentsRequestItem({
    required this.songIdentifier,
    required this.versionName,
    this.instrumentType,
    this.asciiLegend,
    this.asciiContents,
    this.pdfContents,
    this.guitarProContents,
  });

  factory InsertSongContentsRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertSongContentsRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongContentsRequestItemToJson(this);
}

@JsonSerializable()
class InsertSongContentsRequest {
  final List<InsertSongContentsRequestItem> songContents;
  const InsertSongContentsRequest({required this.songContents});

  factory InsertSongContentsRequest.fromJson(final Map<String, dynamic> json) =>
      _$InsertSongContentsRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertSongContentsRequestToJson(this);
}

@JsonSerializable()
class SongContentDelta {
  final String identifier;
  final String versionName;
  final String? instrumentType;
  final String? asciiLegend;
  final String? asciiContents;
  final String? pdfContents;
  final String? guitarProContents;

  const SongContentDelta({
    required this.identifier,
    required this.versionName,
    this.instrumentType,
    this.asciiLegend,
    this.asciiContents,
    this.pdfContents,
    this.guitarProContents,
  });

  factory SongContentDelta.fromJson(final Map<String, dynamic> json) =>
      _$SongContentDeltaFromJson(json);
  Map<String, dynamic> toJson() => _$SongContentDeltaToJson(this);
}

@JsonSerializable()
class SongContentDeltaRequest {
  final List<SongContentDelta> songContentDeltas;
  const SongContentDeltaRequest({required this.songContentDeltas});

  factory SongContentDeltaRequest.fromJson(final Map<String, dynamic> json) =>
      _$SongContentDeltaRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SongContentDeltaRequestToJson(this);
}

@JsonSerializable()
class UpdateSongArtworkOrderRequest {
  final List<UpdateSongArtworkOrderRequestItem> songArtworkOrders;
  const UpdateSongArtworkOrderRequest({required this.songArtworkOrders});
  factory UpdateSongArtworkOrderRequest.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateSongArtworkOrderRequestFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateSongArtworkOrderRequestToJson(this);
}

@JsonSerializable()
class UpdateSongArtworkOrderRequestItem {
  final String identifier;
  final int orderValue;
  const UpdateSongArtworkOrderRequestItem(
      {required this.identifier, required this.orderValue});
  factory UpdateSongArtworkOrderRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateSongArtworkOrderRequestItemFromJson(json);
  Map<String, dynamic> toJson() =>
      _$UpdateSongArtworkOrderRequestItemToJson(this);
}

@JsonSerializable()
class SongArtistsRequest {
  final List<SongArtistsRequestItem> songArtists;
  const SongArtistsRequest({required this.songArtists});

  factory SongArtistsRequest.fromJson(final Map<String, dynamic> json) =>
      _$SongArtistsRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SongArtistsRequestToJson(this);
}

@JsonSerializable()
class SongArtistsRequestItem {
  final String songIdentifier;
  final String artistIdentifier;

  const SongArtistsRequestItem({
    required this.songIdentifier,
    required this.artistIdentifier,
  });

  factory SongArtistsRequestItem.fromJson(final Map<String, dynamic> json) =>
      _$SongArtistsRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$SongArtistsRequestItemToJson(this);
}
