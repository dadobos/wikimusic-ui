import 'package:json_annotation/json_annotation.dart';

part 'login.g.dart';

@JsonSerializable()
class LoginRequest {
  final String wikimusicEmail;
  final String wikimusicPassword;

  const LoginRequest(
      {required this.wikimusicEmail, required this.wikimusicPassword});

  factory LoginRequest.fromJson(final Map<String, dynamic> json) =>
      _$LoginRequestFromJson(json);
  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}

enum UserRole {
  superUser("wm::superuser", "Root"),
  maintainer("wm::maintainer", "Maintainer"),
  lowRank("wm::lowrank", "WikiMusic member"),
  demo("wm::demo", "Demo user");

  final String roleName;
  final String roleDescription;

  const UserRole(this.roleName, this.roleDescription);

  @override
  String toString() => roleName;
}

@JsonSerializable()
class GetMeResponse {
  final String identifier, displayName, emailAddress;
  final List<String> roles;

  const GetMeResponse(
      {required this.identifier,
      required this.displayName,
      required this.emailAddress,
      required this.roles});

  factory GetMeResponse.fromJson(final Map<String, dynamic> json) =>
      _$GetMeResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GetMeResponseToJson(this);
}
