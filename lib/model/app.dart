enum SortOrder {
  displayNameAsc("display-name-asc", "A-Z"),
  displayNameDesc("display-name-desc", "Z-A"),
  createdAtAsc("created-at-asc", "Old to New"),
  createdAtDesc("created-at-desc", "New to Old");

  final String orderName;
  final String orderDescription;

  const SortOrder(this.orderName, this.orderDescription);

  @override
  String toString() => orderName;
}

final Map<String, SortOrder> sorts = {
  SortOrder.displayNameAsc.orderDescription: SortOrder.displayNameAsc,
  SortOrder.displayNameDesc.orderDescription: SortOrder.displayNameDesc,
  SortOrder.createdAtAsc.orderDescription: SortOrder.createdAtAsc,
  SortOrder.createdAtDesc.orderDescription: SortOrder.createdAtDesc,
};
