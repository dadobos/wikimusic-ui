import 'package:json_annotation/json_annotation.dart';
import 'package:wikimusic_ui/model/other.dart';

part 'genres.g.dart';

@JsonSerializable()
class GenreOpinion {
  final String genreIdentifier;
  final Opinion opinion;

  const GenreOpinion({
    required this.opinion,
    required this.genreIdentifier,
  });

  factory GenreOpinion.fromJson(final Map<String, dynamic> json) =>
      _$GenreOpinionFromJson(json);
  Map<String, dynamic> toJson() => _$GenreOpinionToJson(this);
}

@JsonSerializable()
class GenreArtwork {
  final String genreIdentifier;
  final Artwork artwork;

  const GenreArtwork({
    required this.artwork,
    required this.genreIdentifier,
  });

  factory GenreArtwork.fromJson(final Map<String, dynamic> json) =>
      _$GenreArtworkFromJson(json);
  Map<String, dynamic> toJson() => _$GenreArtworkToJson(this);
}

@JsonSerializable()
class Genre {
  final String identifier;
  final String displayName;
  final String createdBy;
  final int visibilityStatus;
  final String? approvedBy;
  final DateTime createdAt;
  final DateTime? lastEditedAt;

  final Map<String, GenreArtwork> artworks;
  final Map<String, GenreOpinion> opinions;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final int viewCount;
  final String? description;

  const Genre(
      {required this.identifier,
      required this.displayName,
      required this.createdBy,
      required this.visibilityStatus,
      this.approvedBy,
      required this.createdAt,
      this.lastEditedAt,
      required this.artworks,
      required this.opinions,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      required this.viewCount,
      this.description});
  factory Genre.fromJson(final Map<String, dynamic> json) =>
      _$GenreFromJson(json);
  Map<String, dynamic> toJson() => _$GenreToJson(this);
}

@JsonSerializable()
class GetGenresQueryResponse {
  final Map<String, Genre> genres;
  final List<String> sortOrder;
  const GetGenresQueryResponse({required this.genres, required this.sortOrder});

  factory GetGenresQueryResponse.fromJson(final Map<String, dynamic> json) =>
      _$GetGenresQueryResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GetGenresQueryResponseToJson(this);
}

@JsonSerializable()
class InsertGenresRequestItem {
  final String displayName;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const InsertGenresRequestItem(
      {required this.displayName,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.description});

  factory InsertGenresRequestItem.fromJson(final Map<String, dynamic> json) =>
      _$InsertGenresRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertGenresRequestItemToJson(this);
}

@JsonSerializable()
class InsertGenresRequest {
  final List<InsertGenresRequestItem> genres;
  const InsertGenresRequest({required this.genres});

  factory InsertGenresRequest.fromJson(final Map<String, dynamic> json) =>
      _$InsertGenresRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertGenresRequestToJson(this);
}

@JsonSerializable()
class GenreDeltaRequest {
  final List<GenreDelta> genreDeltas;
  const GenreDeltaRequest({required this.genreDeltas});

  factory GenreDeltaRequest.fromJson(final Map<String, dynamic> json) =>
      _$GenreDeltaRequestFromJson(json);
  Map<String, dynamic> toJson() => _$GenreDeltaRequestToJson(this);
}

@JsonSerializable()
class GenreDelta {
  final String identifier;
  final String displayName;
  final String? spotifyUrl;
  final String? youtubeUrl;
  final String? soundcloudUrl;
  final String? wikipediaUrl;
  final String? description;

  const GenreDelta(
      {required this.identifier,
      required this.displayName,
      this.spotifyUrl,
      this.youtubeUrl,
      this.soundcloudUrl,
      this.wikipediaUrl,
      this.description});

  factory GenreDelta.fromJson(final Map<String, dynamic> json) =>
      _$GenreDeltaFromJson(json);
  Map<String, dynamic> toJson() => _$GenreDeltaToJson(this);
}

@JsonSerializable()
class InsertGenreArtworksRequest {
  final List<InsertGenreArtworksRequestItem> genreArtworks;
  const InsertGenreArtworksRequest({required this.genreArtworks});

  factory InsertGenreArtworksRequest.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertGenreArtworksRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InsertGenreArtworksRequestToJson(this);
}

@JsonSerializable()
class InsertGenreArtworksRequestItem {
  final String genreIdentifier;
  final String contentUrl;
  final String? contentCaption;
  final int orderValue;

  const InsertGenreArtworksRequestItem(
      {required this.genreIdentifier,
      required this.contentUrl,
      this.contentCaption,
      required this.orderValue});

  factory InsertGenreArtworksRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$InsertGenreArtworksRequestItemFromJson(json);
  Map<String, dynamic> toJson() => _$InsertGenreArtworksRequestItemToJson(this);
}

@JsonSerializable()
class UpdateGenreArtworkOrderRequest {
  final List<UpdateGenreArtworkOrderRequestItem> genreArtworkOrders;
  const UpdateGenreArtworkOrderRequest({required this.genreArtworkOrders});
  factory UpdateGenreArtworkOrderRequest.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateGenreArtworkOrderRequestFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateGenreArtworkOrderRequestToJson(this);
}

@JsonSerializable()
class UpdateGenreArtworkOrderRequestItem {
  final String identifier;
  final int orderValue;
  const UpdateGenreArtworkOrderRequestItem(
      {required this.identifier, required this.orderValue});
  factory UpdateGenreArtworkOrderRequestItem.fromJson(
          final Map<String, dynamic> json) =>
      _$UpdateGenreArtworkOrderRequestItemFromJson(json);
  Map<String, dynamic> toJson() =>
      _$UpdateGenreArtworkOrderRequestItemToJson(this);
}
