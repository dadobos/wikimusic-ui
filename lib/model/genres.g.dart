// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'genres.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenreOpinion _$GenreOpinionFromJson(Map<String, dynamic> json) => GenreOpinion(
      opinion: Opinion.fromJson(json['opinion'] as Map<String, dynamic>),
      genreIdentifier: json['genreIdentifier'] as String,
    );

Map<String, dynamic> _$GenreOpinionToJson(GenreOpinion instance) =>
    <String, dynamic>{
      'genreIdentifier': instance.genreIdentifier,
      'opinion': instance.opinion,
    };

GenreArtwork _$GenreArtworkFromJson(Map<String, dynamic> json) => GenreArtwork(
      artwork: Artwork.fromJson(json['artwork'] as Map<String, dynamic>),
      genreIdentifier: json['genreIdentifier'] as String,
    );

Map<String, dynamic> _$GenreArtworkToJson(GenreArtwork instance) =>
    <String, dynamic>{
      'genreIdentifier': instance.genreIdentifier,
      'artwork': instance.artwork,
    };

Genre _$GenreFromJson(Map<String, dynamic> json) => Genre(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      createdBy: json['createdBy'] as String,
      visibilityStatus: (json['visibilityStatus'] as num).toInt(),
      approvedBy: json['approvedBy'] as String?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
      artworks: (json['artworks'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, GenreArtwork.fromJson(e as Map<String, dynamic>)),
      ),
      opinions: (json['opinions'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, GenreOpinion.fromJson(e as Map<String, dynamic>)),
      ),
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      viewCount: (json['viewCount'] as num).toInt(),
      description: json['description'] as String?,
    );

Map<String, dynamic> _$GenreToJson(Genre instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'createdBy': instance.createdBy,
      'visibilityStatus': instance.visibilityStatus,
      'approvedBy': instance.approvedBy,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
      'artworks': instance.artworks,
      'opinions': instance.opinions,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'viewCount': instance.viewCount,
      'description': instance.description,
    };

GetGenresQueryResponse _$GetGenresQueryResponseFromJson(
        Map<String, dynamic> json) =>
    GetGenresQueryResponse(
      genres: (json['genres'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, Genre.fromJson(e as Map<String, dynamic>)),
      ),
      sortOrder:
          (json['sortOrder'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$GetGenresQueryResponseToJson(
        GetGenresQueryResponse instance) =>
    <String, dynamic>{
      'genres': instance.genres,
      'sortOrder': instance.sortOrder,
    };

InsertGenresRequestItem _$InsertGenresRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertGenresRequestItem(
      displayName: json['displayName'] as String,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$InsertGenresRequestItemToJson(
        InsertGenresRequestItem instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertGenresRequest _$InsertGenresRequestFromJson(Map<String, dynamic> json) =>
    InsertGenresRequest(
      genres: (json['genres'] as List<dynamic>)
          .map((e) =>
              InsertGenresRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertGenresRequestToJson(
        InsertGenresRequest instance) =>
    <String, dynamic>{
      'genres': instance.genres,
    };

GenreDeltaRequest _$GenreDeltaRequestFromJson(Map<String, dynamic> json) =>
    GenreDeltaRequest(
      genreDeltas: (json['genreDeltas'] as List<dynamic>)
          .map((e) => GenreDelta.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GenreDeltaRequestToJson(GenreDeltaRequest instance) =>
    <String, dynamic>{
      'genreDeltas': instance.genreDeltas,
    };

GenreDelta _$GenreDeltaFromJson(Map<String, dynamic> json) => GenreDelta(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$GenreDeltaToJson(GenreDelta instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertGenreArtworksRequest _$InsertGenreArtworksRequestFromJson(
        Map<String, dynamic> json) =>
    InsertGenreArtworksRequest(
      genreArtworks: (json['genreArtworks'] as List<dynamic>)
          .map((e) => InsertGenreArtworksRequestItem.fromJson(
              e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertGenreArtworksRequestToJson(
        InsertGenreArtworksRequest instance) =>
    <String, dynamic>{
      'genreArtworks': instance.genreArtworks,
    };

InsertGenreArtworksRequestItem _$InsertGenreArtworksRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertGenreArtworksRequestItem(
      genreIdentifier: json['genreIdentifier'] as String,
      contentUrl: json['contentUrl'] as String,
      contentCaption: json['contentCaption'] as String?,
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$InsertGenreArtworksRequestItemToJson(
        InsertGenreArtworksRequestItem instance) =>
    <String, dynamic>{
      'genreIdentifier': instance.genreIdentifier,
      'contentUrl': instance.contentUrl,
      'contentCaption': instance.contentCaption,
      'orderValue': instance.orderValue,
    };

UpdateGenreArtworkOrderRequest _$UpdateGenreArtworkOrderRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateGenreArtworkOrderRequest(
      genreArtworkOrders: (json['genreArtworkOrders'] as List<dynamic>)
          .map((e) => UpdateGenreArtworkOrderRequestItem.fromJson(
              e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UpdateGenreArtworkOrderRequestToJson(
        UpdateGenreArtworkOrderRequest instance) =>
    <String, dynamic>{
      'genreArtworkOrders': instance.genreArtworkOrders,
    };

UpdateGenreArtworkOrderRequestItem _$UpdateGenreArtworkOrderRequestItemFromJson(
        Map<String, dynamic> json) =>
    UpdateGenreArtworkOrderRequestItem(
      identifier: json['identifier'] as String,
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$UpdateGenreArtworkOrderRequestItemToJson(
        UpdateGenreArtworkOrderRequestItem instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'orderValue': instance.orderValue,
    };
