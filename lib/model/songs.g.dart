// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'songs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SongOpinion _$SongOpinionFromJson(Map<String, dynamic> json) => SongOpinion(
      opinion: Opinion.fromJson(json['opinion'] as Map<String, dynamic>),
      songIdentifier: json['songIdentifier'] as String,
    );

Map<String, dynamic> _$SongOpinionToJson(SongOpinion instance) =>
    <String, dynamic>{
      'songIdentifier': instance.songIdentifier,
      'opinion': instance.opinion,
    };

SongContent _$SongContentFromJson(Map<String, dynamic> json) => SongContent(
      identifier: json['identifier'] as String,
      songIdentifier: json['songIdentifier'] as String,
      createdBy: json['createdBy'] as String,
      versionName: json['versionName'] as String,
      visibilityStatus: (json['visibilityStatus'] as num).toInt(),
      approvedBy: json['approvedBy'] as String?,
      instrumentType: json['instrumentType'] as String,
      asciiLegend: json['asciiLegend'] as String?,
      asciiContents: json['asciiContents'] as String?,
      pdfContents: json['pdfContents'] as String?,
      guitarProContents: json['guitarProContents'] as String?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
    );

Map<String, dynamic> _$SongContentToJson(SongContent instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'songIdentifier': instance.songIdentifier,
      'createdBy': instance.createdBy,
      'versionName': instance.versionName,
      'visibilityStatus': instance.visibilityStatus,
      'approvedBy': instance.approvedBy,
      'instrumentType': instance.instrumentType,
      'asciiLegend': instance.asciiLegend,
      'asciiContents': instance.asciiContents,
      'pdfContents': instance.pdfContents,
      'guitarProContents': instance.guitarProContents,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
    };

SongArtwork _$SongArtworkFromJson(Map<String, dynamic> json) => SongArtwork(
      artwork: Artwork.fromJson(json['artwork'] as Map<String, dynamic>),
      songIdentifier: json['songIdentifier'] as String,
    );

Map<String, dynamic> _$SongArtworkToJson(SongArtwork instance) =>
    <String, dynamic>{
      'songIdentifier': instance.songIdentifier,
      'artwork': instance.artwork,
    };

Song _$SongFromJson(Map<String, dynamic> json) => Song(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      musicKey: json['musicKey'] as String?,
      musicTuning: json['musicTuning'] as String?,
      musicCreationDate: json['musicCreationDate'] as String?,
      albumName: json['albumName'] as String?,
      albumInfoLink: json['albumInfoLink'] as String?,
      createdBy: json['createdBy'] as String,
      visibilityStatus: (json['visibilityStatus'] as num).toInt(),
      approvedBy: json['approvedBy'] as String?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
      contents: (json['contents'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, SongContent.fromJson(e as Map<String, dynamic>)),
      ),
      artists: Map<String, String>.from(json['artists'] as Map),
      artworks: (json['artworks'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, SongArtwork.fromJson(e as Map<String, dynamic>)),
      ),
      opinions: (json['opinions'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, SongOpinion.fromJson(e as Map<String, dynamic>)),
      ),
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      viewCount: (json['viewCount'] as num).toInt(),
      description: json['description'] as String?,
    );

Map<String, dynamic> _$SongToJson(Song instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'musicKey': instance.musicKey,
      'musicTuning': instance.musicTuning,
      'musicCreationDate': instance.musicCreationDate,
      'albumName': instance.albumName,
      'albumInfoLink': instance.albumInfoLink,
      'createdBy': instance.createdBy,
      'visibilityStatus': instance.visibilityStatus,
      'approvedBy': instance.approvedBy,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
      'artworks': instance.artworks,
      'artists': instance.artists,
      'opinions': instance.opinions,
      'contents': instance.contents,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'viewCount': instance.viewCount,
      'description': instance.description,
    };

GetSongsQueryResponse _$GetSongsQueryResponseFromJson(
        Map<String, dynamic> json) =>
    GetSongsQueryResponse(
      songs: (json['songs'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, Song.fromJson(e as Map<String, dynamic>)),
      ),
      sortOrder:
          (json['sortOrder'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$GetSongsQueryResponseToJson(
        GetSongsQueryResponse instance) =>
    <String, dynamic>{
      'songs': instance.songs,
      'sortOrder': instance.sortOrder,
    };

InsertSongsRequestItem _$InsertSongsRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertSongsRequestItem(
      displayName: json['displayName'] as String,
      musicKey: json['musicKey'] as String?,
      musicTuning: json['musicTuning'] as String?,
      musicCreationDate: json['musicCreationDate'] as String?,
      albumName: json['albumName'] as String?,
      albumInfoLink: json['albumInfoLink'] as String?,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$InsertSongsRequestItemToJson(
        InsertSongsRequestItem instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'musicKey': instance.musicKey,
      'musicTuning': instance.musicTuning,
      'musicCreationDate': instance.musicCreationDate,
      'albumName': instance.albumName,
      'albumInfoLink': instance.albumInfoLink,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertSongsRequest _$InsertSongsRequestFromJson(Map<String, dynamic> json) =>
    InsertSongsRequest(
      songs: (json['songs'] as List<dynamic>)
          .map(
              (e) => InsertSongsRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertSongsRequestToJson(InsertSongsRequest instance) =>
    <String, dynamic>{
      'songs': instance.songs,
    };

SongDeltaRequest _$SongDeltaRequestFromJson(Map<String, dynamic> json) =>
    SongDeltaRequest(
      songDeltas: (json['songDeltas'] as List<dynamic>)
          .map((e) => SongDelta.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SongDeltaRequestToJson(SongDeltaRequest instance) =>
    <String, dynamic>{
      'songDeltas': instance.songDeltas,
    };

SongDelta _$SongDeltaFromJson(Map<String, dynamic> json) => SongDelta(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      spotifyUrl: json['spotifyUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      soundcloudUrl: json['soundcloudUrl'] as String?,
      wikipediaUrl: json['wikipediaUrl'] as String?,
      musicKey: json['musicKey'] as String?,
      musicTuning: json['musicTuning'] as String?,
      musicCreationDate: json['musicCreationDate'] as String?,
      albumName: json['albumName'] as String?,
      albumInfoLink: json['albumInfoLink'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$SongDeltaToJson(SongDelta instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'musicKey': instance.musicKey,
      'musicTuning': instance.musicTuning,
      'musicCreationDate': instance.musicCreationDate,
      'albumName': instance.albumName,
      'albumInfoLink': instance.albumInfoLink,
      'spotifyUrl': instance.spotifyUrl,
      'youtubeUrl': instance.youtubeUrl,
      'soundcloudUrl': instance.soundcloudUrl,
      'wikipediaUrl': instance.wikipediaUrl,
      'description': instance.description,
    };

InsertSongArtworksRequest _$InsertSongArtworksRequestFromJson(
        Map<String, dynamic> json) =>
    InsertSongArtworksRequest(
      songArtworks: (json['songArtworks'] as List<dynamic>)
          .map((e) =>
              InsertSongArtworksRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertSongArtworksRequestToJson(
        InsertSongArtworksRequest instance) =>
    <String, dynamic>{
      'songArtworks': instance.songArtworks,
    };

InsertSongArtworksRequestItem _$InsertSongArtworksRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertSongArtworksRequestItem(
      songIdentifier: json['songIdentifier'] as String,
      contentUrl: json['contentUrl'] as String,
      contentCaption: json['contentCaption'] as String?,
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$InsertSongArtworksRequestItemToJson(
        InsertSongArtworksRequestItem instance) =>
    <String, dynamic>{
      'songIdentifier': instance.songIdentifier,
      'contentUrl': instance.contentUrl,
      'contentCaption': instance.contentCaption,
      'orderValue': instance.orderValue,
    };

InsertSongContentsRequestItem _$InsertSongContentsRequestItemFromJson(
        Map<String, dynamic> json) =>
    InsertSongContentsRequestItem(
      songIdentifier: json['songIdentifier'] as String,
      versionName: json['versionName'] as String,
      instrumentType: json['instrumentType'] as String?,
      asciiLegend: json['asciiLegend'] as String?,
      asciiContents: json['asciiContents'] as String?,
      pdfContents: json['pdfContents'] as String?,
      guitarProContents: json['guitarProContents'] as String?,
    );

Map<String, dynamic> _$InsertSongContentsRequestItemToJson(
        InsertSongContentsRequestItem instance) =>
    <String, dynamic>{
      'songIdentifier': instance.songIdentifier,
      'versionName': instance.versionName,
      'instrumentType': instance.instrumentType,
      'asciiLegend': instance.asciiLegend,
      'asciiContents': instance.asciiContents,
      'pdfContents': instance.pdfContents,
      'guitarProContents': instance.guitarProContents,
    };

InsertSongContentsRequest _$InsertSongContentsRequestFromJson(
        Map<String, dynamic> json) =>
    InsertSongContentsRequest(
      songContents: (json['songContents'] as List<dynamic>)
          .map((e) =>
              InsertSongContentsRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InsertSongContentsRequestToJson(
        InsertSongContentsRequest instance) =>
    <String, dynamic>{
      'songContents': instance.songContents,
    };

SongContentDelta _$SongContentDeltaFromJson(Map<String, dynamic> json) =>
    SongContentDelta(
      identifier: json['identifier'] as String,
      versionName: json['versionName'] as String,
      instrumentType: json['instrumentType'] as String?,
      asciiLegend: json['asciiLegend'] as String?,
      asciiContents: json['asciiContents'] as String?,
      pdfContents: json['pdfContents'] as String?,
      guitarProContents: json['guitarProContents'] as String?,
    );

Map<String, dynamic> _$SongContentDeltaToJson(SongContentDelta instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'versionName': instance.versionName,
      'instrumentType': instance.instrumentType,
      'asciiLegend': instance.asciiLegend,
      'asciiContents': instance.asciiContents,
      'pdfContents': instance.pdfContents,
      'guitarProContents': instance.guitarProContents,
    };

SongContentDeltaRequest _$SongContentDeltaRequestFromJson(
        Map<String, dynamic> json) =>
    SongContentDeltaRequest(
      songContentDeltas: (json['songContentDeltas'] as List<dynamic>)
          .map((e) => SongContentDelta.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SongContentDeltaRequestToJson(
        SongContentDeltaRequest instance) =>
    <String, dynamic>{
      'songContentDeltas': instance.songContentDeltas,
    };

UpdateSongArtworkOrderRequest _$UpdateSongArtworkOrderRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateSongArtworkOrderRequest(
      songArtworkOrders: (json['songArtworkOrders'] as List<dynamic>)
          .map((e) => UpdateSongArtworkOrderRequestItem.fromJson(
              e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UpdateSongArtworkOrderRequestToJson(
        UpdateSongArtworkOrderRequest instance) =>
    <String, dynamic>{
      'songArtworkOrders': instance.songArtworkOrders,
    };

UpdateSongArtworkOrderRequestItem _$UpdateSongArtworkOrderRequestItemFromJson(
        Map<String, dynamic> json) =>
    UpdateSongArtworkOrderRequestItem(
      identifier: json['identifier'] as String,
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$UpdateSongArtworkOrderRequestItemToJson(
        UpdateSongArtworkOrderRequestItem instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'orderValue': instance.orderValue,
    };

SongArtistsRequest _$SongArtistsRequestFromJson(Map<String, dynamic> json) =>
    SongArtistsRequest(
      songArtists: (json['songArtists'] as List<dynamic>)
          .map(
              (e) => SongArtistsRequestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SongArtistsRequestToJson(SongArtistsRequest instance) =>
    <String, dynamic>{
      'songArtists': instance.songArtists,
    };

SongArtistsRequestItem _$SongArtistsRequestItemFromJson(
        Map<String, dynamic> json) =>
    SongArtistsRequestItem(
      songIdentifier: json['songIdentifier'] as String,
      artistIdentifier: json['artistIdentifier'] as String,
    );

Map<String, dynamic> _$SongArtistsRequestItemToJson(
        SongArtistsRequestItem instance) =>
    <String, dynamic>{
      'songIdentifier': instance.songIdentifier,
      'artistIdentifier': instance.artistIdentifier,
    };
