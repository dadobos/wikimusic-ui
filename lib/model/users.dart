import 'package:json_annotation/json_annotation.dart';

part 'users.g.dart';

@JsonSerializable()
class InviteUsersRequest {
  final String displayName, email, role;

  const InviteUsersRequest(
      {required this.displayName, required this.email, required this.role});

  factory InviteUsersRequest.fromJson(final Map<String, dynamic> json) =>
      _$InviteUsersRequestFromJson(json);
  Map<String, dynamic> toJson() => _$InviteUsersRequestToJson(this);
}

@JsonSerializable()
class DeleteUsersRequest {
  final String email;

  const DeleteUsersRequest({required this.email});

  factory DeleteUsersRequest.fromJson(final Map<String, dynamic> json) =>
      _$DeleteUsersRequestFromJson(json);
  Map<String, dynamic> toJson() => _$DeleteUsersRequestToJson(this);
}

@JsonSerializable()
class DoPasswordResetRequest {
  final String email, token, password, passwordConfirm;

  const DoPasswordResetRequest(
      {required this.email,
      required this.token,
      required this.password,
      required this.passwordConfirm});

  factory DoPasswordResetRequest.fromJson(final Map<String, dynamic> json) =>
      _$DoPasswordResetRequestFromJson(json);
  Map<String, dynamic> toJson() => _$DoPasswordResetRequestToJson(this);
}
