// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'other.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Opinion _$OpinionFromJson(Map<String, dynamic> json) => Opinion(
      identifier: json['identifier'] as String,
      createdBy: json['createdBy'] as String,
      isLike: json['isLike'] as bool,
      isDislike: json['isDislike'] as bool,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
    );

Map<String, dynamic> _$OpinionToJson(Opinion instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'createdBy': instance.createdBy,
      'isLike': instance.isLike,
      'isDislike': instance.isDislike,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
    };

Artwork _$ArtworkFromJson(Map<String, dynamic> json) => Artwork(
      identifier: json['identifier'] as String,
      createdBy: json['createdBy'] as String,
      visibilityStatus: (json['visibilityStatus'] as num).toInt(),
      approvedBy: json['approvedBy'] as String?,
      contentUrl: json['contentUrl'] as String,
      contentCaption: json['contentCaption'] as String?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      lastEditedAt: json['lastEditedAt'] == null
          ? null
          : DateTime.parse(json['lastEditedAt'] as String),
      orderValue: (json['orderValue'] as num).toInt(),
    );

Map<String, dynamic> _$ArtworkToJson(Artwork instance) => <String, dynamic>{
      'identifier': instance.identifier,
      'createdBy': instance.createdBy,
      'visibilityStatus': instance.visibilityStatus,
      'approvedBy': instance.approvedBy,
      'contentUrl': instance.contentUrl,
      'contentCaption': instance.contentCaption,
      'createdAt': instance.createdAt.toIso8601String(),
      'lastEditedAt': instance.lastEditedAt?.toIso8601String(),
      'orderValue': instance.orderValue,
    };
