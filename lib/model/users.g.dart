// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InviteUsersRequest _$InviteUsersRequestFromJson(Map<String, dynamic> json) =>
    InviteUsersRequest(
      displayName: json['displayName'] as String,
      email: json['email'] as String,
      role: json['role'] as String,
    );

Map<String, dynamic> _$InviteUsersRequestToJson(InviteUsersRequest instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'email': instance.email,
      'role': instance.role,
    };

DeleteUsersRequest _$DeleteUsersRequestFromJson(Map<String, dynamic> json) =>
    DeleteUsersRequest(
      email: json['email'] as String,
    );

Map<String, dynamic> _$DeleteUsersRequestToJson(DeleteUsersRequest instance) =>
    <String, dynamic>{
      'email': instance.email,
    };

DoPasswordResetRequest _$DoPasswordResetRequestFromJson(
        Map<String, dynamic> json) =>
    DoPasswordResetRequest(
      email: json['email'] as String,
      token: json['token'] as String,
      password: json['password'] as String,
      passwordConfirm: json['passwordConfirm'] as String,
    );

Map<String, dynamic> _$DoPasswordResetRequestToJson(
        DoPasswordResetRequest instance) =>
    <String, dynamic>{
      'email': instance.email,
      'token': instance.token,
      'password': instance.password,
      'passwordConfirm': instance.passwordConfirm,
    };
