import 'package:flutter/material.dart';

class AppLocalization {
  final RouteNames routeNames;

  final Placeholders placeholders;
  final Titles titles;
  final Buttons buttons;

  final String views;
  final Forms forms;

  const AppLocalization(
      {required this.placeholders,
      required this.buttons,
      required this.titles,
      required this.routeNames,
      required this.views,
      required this.forms});
}

class Placeholders {
  final String email, password, url;
  final String search;

  const Placeholders(
      {required this.email,
      required this.password,
      required this.url,
      required this.search});
}

class Buttons {
  final String submit, reset;

  const Buttons({required this.submit, required this.reset});
}

class RouteNames {
  final String preferences, login, songs, genres, artists, management;
  final String newSong, newGenre, newArtist;
  final String editSong, editGenre, editArtist;
  final String newSongContent, editSongContent;

  const RouteNames(
      {required this.preferences,
      required this.login,
      required this.songs,
      required this.genres,
      required this.artists,
      required this.management,
      required this.newSong,
      required this.newGenre,
      required this.newArtist,
      required this.editSong,
      required this.editArtist,
      required this.editGenre,
      required this.newSongContent,
      required this.editSongContent});
}

class Titles {
  final String resetPassword;
  final String login;

  const Titles({required this.login, required this.resetPassword});
}

class Forms {
  final String badData, goodSubmission;

  const Forms({required this.badData, required this.goodSubmission});
}

AppLocalization translationDictionaryFromLocale(final String locale) {
  switch (locale) {
    case "nl":
      return nlAppLocalization;
    default:
      return enAppLocalization;
  }
}

AppLocalization nlAppLocalization = const AppLocalization(
    buttons: Buttons(submit: "klaar", reset: "herstellen"),
    routeNames: RouteNames(
        preferences: "Instellingen",
        login: "Inloggen",
        songs: "Nummers",
        genres: "Genres",
        artists: "Artiesten",
        management: "Beheer",
        newSong: "Nieuwe nummer",
        newGenre: "Nieuwe genre",
        newArtist: "Nieuwe artiest",
        editSong: "Nummer bewerken",
        editArtist: "Artiest bewerken",
        editGenre: "Genre bewerken",
        newSongContent: "Nieuwe nummer inhoud",
        editSongContent: "Nummer inhoud aanpassen"),
    placeholders: Placeholders(
        email: "Uw emailaddres",
        password: "Uw wachtwoord",
        url: "Een geldige URL",
        search: "Zoeken"),
    titles: Titles(
      resetPassword: "Wachtwoord vergeten?",
      login: "Inloggen op WikiMusic",
    ),
    views: "weergaven",
    forms: Forms(
        badData: "Ongeldige waarde!",
        goodSubmission: "Formulier succesvol ingediend!"));
AppLocalization enAppLocalization = const AppLocalization(
    buttons: Buttons(submit: "submit", reset: "reset"),
    routeNames: RouteNames(
        preferences: "Preferences",
        login: "Log in",
        songs: "Songs",
        genres: "Genres",
        artists: "Artists",
        management: "Management",
        newSong: "New song",
        newGenre: "New genre",
        newArtist: "New artists",
        editSong: "Edit song",
        editArtist: "Edit artists",
        editGenre: "Edit genre",
        newSongContent: "New song content",
        editSongContent: "Edit song content"),
    placeholders: Placeholders(
        email: "Your e-mail address",
        password: "Your password",
        url: "A valid URL",
        search: "Search"),
    titles: Titles(
      resetPassword: "Forgot password?",
      login: "Log in to WikiMusic",
    ),
    views: "views",
    forms: Forms(
        badData: "Invalid value!",
        goodSubmission: "Successfully submitted form!"));

Map<String, AppLocalization> dictionaries = Map.fromEntries(
    [MapEntry("en", enAppLocalization), MapEntry("nl", nlAppLocalization)]);

class LanguageSettings {
  const LanguageSettings({required this.appLocalization, required this.locale});
  final AppLocalization appLocalization;
  final String locale;
}

class LanguageChanged extends Notification {
  LanguageChanged({required this.languageSettings});
  final LanguageSettings languageSettings;
}

class TranslationProvider extends InheritedWidget {
  final ValueNotifier<LanguageSettings> languageSettings;

  const TranslationProvider(
      {super.key, required super.child, required this.languageSettings});

  static TranslationProvider? maybeOf(final BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<TranslationProvider>();
  }

  static TranslationProvider of(final BuildContext context) {
    final TranslationProvider? result = maybeOf(context);
    assert(result != null, 'No L10NProvider found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(covariant final TranslationProvider oldWidget) {
    return oldWidget.languageSettings != languageSettings;
  }
}
