import 'dart:async';
import 'dart:io';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/app.dart';

import 'package:wikimusic_ui/model/songs.dart';

Future<GetSongsQueryResponse> fetchSongs(
    final Env env, final SortOrder sorting, final int offset) async {
  final r = await doHttp(env,
      "${env.apiUrl}/songs?sort-order=$sorting&include=comments,opinions,artworks,artists&limit=10&offset=$offset",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetSongsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr;
  } else {
    throw Exception('Failed to load songs');
  }
}

Future<GetSongsQueryResponse> searchSongs(final Env env,
    final SortOrder sorting, final int offset, final String? s) async {
  final r = await doHttp(env,
      "${env.apiUrl}/songs/search/$s?sort-order=$sorting&include=comments,opinions,artworks,artists&limit=10&offset=$offset",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetSongsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr;
  } else {
    throw Exception('Failed to load songs');
  }
}

Future<Song> fetchSongByIdentifier(final Env env, final String id) async {
  final r = await doHttp(env,
      "${env.apiUrl}/songs/identifier/$id?include=comments,opinions,artworks,artists,contents",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetSongsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr.songs.values.first;
  } else {
    throw Exception('Failed to load songs');
  }
}
