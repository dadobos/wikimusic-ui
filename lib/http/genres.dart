import 'dart:async';
import 'dart:io';
import 'package:logging/logging.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/app.dart';

import 'package:wikimusic_ui/model/genres.dart';

Future<GetGenresQueryResponse> fetchGenres(
    final Env env, final SortOrder sorting, final int offset) async {
  final r = await doHttp(env,
      "${env.apiUrl}/genres?sort-order=$sorting&include=comments,opinions,artworks&limit=10&offset=$offset",
      authToken: env.authToken);
  if (r.statusCode == HttpStatus.ok) {
    final rr = GetGenresQueryResponse.fromJson(r.data as Map<String, dynamic>);
    Logger('http/genres').info("fetched genres ${rr.sortOrder}");
    return rr;
  } else {
    throw Exception('Failed to load genres');
  }
}

Future<GetGenresQueryResponse> searchGenres(final Env env,
    final SortOrder sorting, final int offset, final String? s) async {
  final r = await doHttp(env,
      "${env.apiUrl}/genres/search/$s?sort-order=$sorting&include=comments,opinions,artworks&limit=10&offset=$offset",
      authToken: env.authToken);
  if (r.statusCode == HttpStatus.ok) {
    final rr = GetGenresQueryResponse.fromJson(r.data as Map<String, dynamic>);
    Logger('http/genres').info("fetched genres ${rr.sortOrder}");
    return rr;
  } else {
    throw Exception('Failed to load genres');
  }
}

Future<Genre> fetchGenreByIdentifier(final Env env, final String id) async {
  final r = await doHttp(env,
      "${env.apiUrl}/genres/identifier/$id?include=comments,opinions,artworks",
      authToken: env.authToken);
  if (r.statusCode == HttpStatus.ok) {
    final rr = GetGenresQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr.genres.values.first;
  } else {
    throw Exception('Failed to load genres');
  }
}
