import 'dart:async';
import 'dart:io';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/login.dart';

Future<void> doLoginCall(
    final Env env, final String username, final String password) async {
  Logger('http/login').info("${env.apiUrl}/login");
  final r = await doHttp(env, "${env.apiUrl}/login",
      data: LoginRequest(wikimusicEmail: username, wikimusicPassword: password)
          .toJson(),
      method: "POST");

  final String? authToken = extractJWT(r);

  final SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('authToken', authToken ?? "");
  await prefs.setString('lastEmailUsed', username);
  env.authToken = authToken;
  env.lastEmailUsed = username;
}

Future<void> doResetPasswordCall(final Env env, final String username) async {
  await doHttp(env, "${env.apiUrl}/reset-password/email/$username",
      method: "POST");
}

Future<GetMeResponse> meCall(final Env env) async {
  final r = await doHttp(env, "${env.apiUrl}/me", authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetMeResponse.fromJson(r.data as Map<String, dynamic>);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('roles', rr.roles);
    await prefs.setString('displayName', rr.displayName);
    await prefs.setString('lastEmailUsed', rr.emailAddress);
    await prefs.setString('identifier', rr.identifier);
    return rr;
  } else {
    throw Exception('Failed to load genres');
  }
}
