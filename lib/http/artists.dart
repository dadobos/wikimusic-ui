import 'dart:async';
import 'dart:io';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/app.dart';

import 'package:wikimusic_ui/model/artists.dart';

Future<GetArtistsQueryResponse> fetchArtists(
    final Env env, final SortOrder sorting, final int offset) async {
  final r = await doHttp(env,
      "${env.apiUrl}/artists?sort-order=$sorting&include=comments,opinions,artworks&limit=10&offset=$offset",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetArtistsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr;
  } else {
    throw Exception('Failed to load artists');
  }
}

Future<GetArtistsQueryResponse> searchArtists(final Env env,
    final SortOrder sorting, final int offset, final String? s) async {
  final r = await doHttp(env,
      "${env.apiUrl}/artists/search/$s?sort-order=$sorting&include=comments,opinions,artworks&limit=10&offset=$offset",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetArtistsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr;
  } else {
    throw Exception('Failed to load artists');
  }
}

Future<Artist> fetchArtistByIdentifier(final Env env, final String id) async {
  final r = await doHttp(env,
      "${env.apiUrl}/artists/identifier/$id?include=comments,opinions,artworks",
      authToken: env.authToken);

  if (r.statusCode == HttpStatus.ok) {
    final rr = GetArtistsQueryResponse.fromJson(r.data as Map<String, dynamic>);
    return rr.artists.values.first;
  } else {
    throw Exception('Failed to load artists');
  }
}
