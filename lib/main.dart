import 'package:flutter/material.dart';
import 'package:wikimusic_ui/bootstrap.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';
import 'package:wikimusic_ui/l10n/l10n.dart';
import 'package:wikimusic_ui/router.dart';
import 'package:wikimusic_ui/theme/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final Env env = await buildEnv();
  final storedPrefs = await UserPreferences.fromStorage();
  runApp(WikiMusicApp(env: env, storedPrefs: storedPrefs));
}

class WikiMusicApp extends StatefulWidget {
  final Env env;
  final UserPreferences storedPrefs;

  const WikiMusicApp({super.key, required this.env, required this.storedPrefs});

  @override
  State<WikiMusicApp> createState() => WikiMusicAppState();
}

class WikiMusicAppState extends State<WikiMusicApp> {
  @override
  Widget build(final BuildContext context) {
    final ValueNotifier<ThemeSettings> themeSettingsNotifier = ValueNotifier(
        ThemeSettings(
            sourceColor: const Color.fromARGB(87, 34, 114, 0),
            // themeMode: ThemeMode.system,
            themeMode: (widget.env.prefersDark ?? false)
                ? ThemeMode.dark
                : ThemeMode.light));

    final String locale = widget.env.locale ?? "en";
    final ValueNotifier<LanguageSettings> languageSettingsNotifier =
        ValueNotifier(LanguageSettings(
            appLocalization: dictionaries[locale] ?? enAppLocalization,
            locale: locale));

    final ValueNotifier<UserPreferences> userPreferencesNotifier =
        ValueNotifier(widget.storedPrefs);

    MaterialApp appBuilder(final BuildContext context) {
      final ThemeProvider theme = ThemeProvider.of(context);

      return MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routerConfig: makeRouter(widget.env),
        title: 'WikiMusic',
        theme: theme.light(themeSettingsNotifier.value.sourceColor),
        darkTheme: theme.dark(themeSettingsNotifier.value.sourceColor),
        themeMode: theme.themeMode(),
      );
    }

    Widget doBuildApp(final MaterialApp Function(BuildContext) eff) {
      return withUserPreferences(
          userPreferencesNotifier,
          (final _, final __, final ___) => withTranslations(
              languageSettingsNotifier,
              (final _, final __, final ___) => withTheme(
                  themeSettingsNotifier,
                  (final buildContext, final __, final ___) =>
                      eff(buildContext))));
    }

    return doBuildApp((final buildContext) => appBuilder(buildContext));
  }
}
