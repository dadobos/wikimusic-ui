import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wikimusic_ui/theme/theme.dart';

class BrightnessToggle extends StatelessWidget {
  const BrightnessToggle({super.key});

  @override
  Widget build(final BuildContext context) => SegmentedButton(
      segments: const <ButtonSegment<bool>>[
        ButtonSegment(
            value: true, icon: Icon(Icons.brightness_3), label: Text("dark")),
        ButtonSegment(
            value: false, icon: Icon(Icons.brightness_7), label: Text("light"))
      ],
      style: ElevatedButton.styleFrom(),
      selected: <bool>{Theme.of(context).brightness == Brightness.dark},
      onSelectionChanged: (final Set<bool> newSelection) async {
        final prefs = await SharedPreferences.getInstance();
        prefs.setBool("prefersDark", newSelection.first);

        // ignore: use_build_context_synchronously
        final settings = ThemeProvider.of(context).settings.value;

        final newSettings = ThemeSettings(
          sourceColor: settings.sourceColor,
          themeMode: newSelection.first ? ThemeMode.dark : ThemeMode.light,
        );
        // ignore: use_build_context_synchronously
        ThemeSettingChange(settings: newSettings).dispatch(context);
        // ignore: use_build_context_synchronously
        context.go("/preferences");
      });
}
