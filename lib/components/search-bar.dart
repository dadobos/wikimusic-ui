import 'package:flutter/material.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';

class WikiSearchBar extends StatefulWidget {
  const WikiSearchBar({super.key, required this.onChanged});
  final ValueChanged<String> onChanged;

  @override
  State<WikiSearchBar> createState() => WikiSearchBarState();
}

class WikiSearchBarState extends State<WikiSearchBar> {
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(final BuildContext context) => SizedBox(
      width: MediaQuery.of(context).size.width > 600
          ? (MediaQuery.of(context).size.width * 0.4)
          : (MediaQuery.of(context).size.width * 0.8),
      child: TextField(
        controller: searchController,
        decoration: InputDecoration(
            border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(16))),
            labelText: t(context).placeholders.search),
        onChanged: widget.onChanged,
      ));
}
