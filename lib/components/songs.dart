import 'dart:convert';

import 'package:download/download.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/http.dart';
import 'package:wikimusic_ui/model/songs.dart';

String mkContentKey(final SongContent? e) {
  if (e == null) {
    return "";
  } else {
    return "${e.versionName} ${e.instrumentType}";
  }
}

class SongVersionView extends StatefulWidget {
  final Song song;
  final Env env;
  final VoidCallback refetchEntity;
  const SongVersionView(
      {super.key,
      required this.song,
      required this.env,
      required this.refetchEntity});

  @override
  State<SongVersionView> createState() => SongVersionViewState();
}

class SongVersionViewState extends State<SongVersionView> {
  String? selectedVersion;
  String? selectedF = "M";

  Future<void> onDownloadPdf(final BuildContext context,
      final String? base64Str, final String displayName) async {
    final stream = Stream.fromIterable(base64Decode(
        base64Str?.replaceAll("data:application/pdf;base64,", "") ?? ""));
    final newFile = '$displayName-${DateTime.now()}.pdf';
    download(stream, newFile);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text("Downloaded file to: $newFile")),
    );
  }

  void setFirstVer() {
    setState(() {
      final List<SongContent> sortedContents =
          widget.song.contents.values.toList();
      sortedContents
          .sort((final a, final b) => a.versionName.compareTo(b.versionName));
      selectedVersion = sortedContents.firstOrNull?.identifier;
    });
  }

  @override
  void initState() {
    super.initState();
    setFirstVer();
  }

  @override
  Widget build(final BuildContext context) {
    final Widget versionDropdown = DropdownMenu<String>(
      initialSelection: selectedVersion,
      dropdownMenuEntries:
          widget.song.contents.entries.map<DropdownMenuEntry<String>>(
        (final e) {
          return DropdownMenuEntry<String>(
              value: e.value.identifier, label: (mkContentKey(e.value)));
        },
      ).toList(),
      onSelected: (final value) {
        setState(() {
          selectedVersion = value;
        });
      },
    );

    final Map<String, double> fontSizes = Map.fromEntries([
      MapEntry("XS", (Theme.of(context).textTheme.bodySmall!.fontSize! - 1)),
      MapEntry("S", Theme.of(context).textTheme.bodySmall!.fontSize!),
      MapEntry("M", Theme.of(context).textTheme.bodyMedium!.fontSize!),
      MapEntry("L", Theme.of(context).textTheme.bodyLarge!.fontSize!),
      MapEntry("XL", Theme.of(context).textTheme.headlineSmall!.fontSize!),
      MapEntry("XXL", Theme.of(context).textTheme.headlineMedium!.fontSize!),
    ].whereType<MapEntry<String, double>>());

    final Widget selectedFontDropdown = DropdownMenu<double>(
        initialSelection: fontSizes[selectedF],
        dropdownMenuEntries:
            fontSizes.entries.map<DropdownMenuEntry<double>>((final e) {
          return DropdownMenuEntry<double>(value: e.value, label: (e.key));
        }).toList(),
        onSelected: (final value) => {
              setState(() {
                selectedF = fontSizes.entries
                    .where((final element) => element.value == value)
                    .first
                    .key;
              })
            });

    final e = widget.song.contents.entries
        .where((final e) => e.value.identifier == selectedVersion)
        .first
        .value;
    final c = Column(
      children: [
        verticalSpacer(),
        header1(
          context,
          "Viewing: ${mkContentKey(e)}",
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            primaryButton(() {
              context.go(
                  "/management/songs/${widget.song.identifier}/contents/edit/${e.identifier}");
            }, "Edit ${mkContentKey(e)}"),
            deleteButton(context, "Delete song content version?", () async {
              await doHttp(widget.env,
                  "${widget.env.apiUrl}/songs/contents/${e.identifier}",
                  authToken: widget.env.authToken, method: "DELETE");
              widget.refetchEntity();
              setFirstVer();
            }),
          ],
        ),
        verticalSpacer(),
        selectedFontDropdown,
        verticalSpacer(),
        if (e.asciiLegend != null)
          ExpansionTile(
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [header3(context, "ASCII legend")],
            ),
            children: [
              Builder(builder: (final BuildContext context) {
                return Column(
                  children: [
                    padding(monospacedText(
                        e.asciiLegend ?? "", fontSizes[selectedF])),
                  ],
                );
              })
            ],
          ),
        if (e.asciiContents != null)
          ExpansionTile(
            initiallyExpanded: true,
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [header2(context, "ASCII contents")],
            ),
            children: [
              Builder(builder: (final BuildContext context) {
                return Column(
                  children: [
                    padding(monospacedText(
                        e.asciiContents ?? "", fontSizes[selectedF])),
                  ],
                );
              })
            ],
          ),
        if (e.pdfContents != null &&
            e.pdfContents != "data:application/octet-stream;base64,")
          Column(
            children: [
              header3(context, "PDF contents"),
              padding(primaryButton(
                  () => onDownloadPdf(
                      context, e.pdfContents, widget.song.displayName),
                  "Download PDF for: ${widget.song.displayName}")),
              verticalSpacer(),
            ],
          ),
      ],
    );
    return Column(
      children: [versionDropdown, verticalSpacer(), c],
    );
  }
}
