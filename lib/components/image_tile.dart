import 'package:flutter/material.dart';
import 'package:wikimusic_ui/components/outlined_card.dart';
import 'package:wikimusic_ui/theme/extensions.dart';

class ImageTile extends StatelessWidget {
  const ImageTile({
    super.key,
    required this.image,
    required this.title,
    required this.subtitle,
  });

  final String image;
  final String title;
  final String subtitle;

  @override
  Widget build(final BuildContext context) => OutlinedCard(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            children: [
              Image.asset(image, fit: BoxFit.cover),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: context.titleSmall!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  )),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(
                  subtitle,
                  overflow: TextOverflow.ellipsis,
                  style: context.labelSmall,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          )
        ]),
      );
}
