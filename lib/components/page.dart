import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wikimusic_ui/components/app.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/dsl/preferences-service.dart';

class MyPage extends StatefulWidget {
  final Env env;
  final BuildContext context;
  final String title;
  final Widget child;
  final bool? hasBackButton;
  final String? backButtonDestination;
  final Decoration? decoration;
  final bool? isLoggedIn;

  final String routeName;

  const MyPage(
      {super.key,
      required this.env,
      required this.context,
      required this.title,
      required this.child,
      this.hasBackButton,
      this.backButtonDestination,
      this.isLoggedIn,
      this.decoration,
      required this.routeName});

  @override
  State<StatefulWidget> createState() {
    return MyPageState();
  }

  int mkLoggedInIndex(final String routeName) {
    switch (routeName) {
      case "artists":
        return 1;
      case "genres":
        return 2;
      case "preferences":
        return 4;
      case "management":
        return 3;
      case "about":
        return 5;
      default:
        return 0;
    }
  }

  int mkNoSessionIndex(final String routeName) {
    switch (routeName) {
      case "about":
        return 1;
      default:
        return 0;
    }
  }
}

class MyPageState extends State<MyPage> {
  @override
  Widget build(final BuildContext context) {
    final Widget appTitle = Row(children: [
      const Icon(Icons.music_note),
      horizontalSpacer(width: 10),
      const Text("WikiMusic", style: TextStyle(fontWeight: FontWeight.bold)),
      horizontalSpacer(width: 10),
      Text(widget.title)
    ]);

    void goBackFunc(final String destination) {
      context.go(destination);
    }

    Widget? leading;
    if (widget.hasBackButton != null && widget.hasBackButton == true) {
      leading = BackButton(
        onPressed: () => goBackFunc(widget.backButtonDestination!),
      );
    }

    int computeSelectedIndex() {
      if (widget.isLoggedIn == true) {
        return widget.mkLoggedInIndex(widget.routeName);
      } else {
        return widget.mkNoSessionIndex(widget.routeName);
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: appTitle,
          leading: leading,
        ),
        bottomNavigationBar: NavigationBar(
          labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
          onDestinationSelected: (final int index) {
            setState(() {
              if (widget.isLoggedIn == true) {
                switch (index) {
                  case 1:
                    context.go("/artists");
                    break;
                  case 2:
                    context.go("/genres");
                    break;
                  case 3:
                    context.go("/management");
                    break;
                  case 4:
                    context.go("/preferences");
                    break;
                  case 5:
                    context.go("/about");
                    break;
                  default:
                    context.go("/songs");
                }
              } else {
                switch (index) {
                  case 1:
                    context.go("/about");
                    break;
                  default:
                    context.go("/login");
                }
              }
            });
          },
          selectedIndex: computeSelectedIndex(),
          destinations: <Widget>[
            if (widget.isLoggedIn == true)
              NavigationDestination(
                icon: const Icon(Icons.music_note),
                label: t(context).routeNames.songs,
              ),
            if (widget.isLoggedIn == true)
              NavigationDestination(
                icon: const Icon(Icons.person),
                label: t(context).routeNames.artists,
              ),
            if (widget.isLoggedIn == true)
              NavigationDestination(
                icon: const Icon(Icons.library_books),
                label: t(context).routeNames.genres,
              ),
            if (widget.isLoggedIn == true)
              NavigationDestination(
                icon: const Icon(Icons.book),
                label: t(context).routeNames.management,
              ),
            if (widget.isLoggedIn == true)
              NavigationDestination(
                icon: const Icon(Icons.settings),
                label: t(context).routeNames.preferences,
              ),
            if (widget.isLoggedIn == false || widget.isLoggedIn == null)
              NavigationDestination(
                icon: const Icon(Icons.account_circle),
                label: t(context).routeNames.login,
              ),
            const NavigationDestination(
              icon: Icon(Icons.question_mark),
              label: "About",
            ),
          ],
        ),
        body: Center(
          child: Container(decoration: widget.decoration, child: widget.child),
        ),
        drawer: Drawer(
            clipBehavior: Clip.hardEdge,
            child: Column(children: [
              DrawerHeader(
                  margin: EdgeInsets.zero,
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
                  child: Align(
                      heightFactor: 2.0,
                      alignment: Alignment.topLeft,
                      child: appTitle)),
              Expanded(
                  child: ListView(children: [
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      title: Text(t(context).routeNames.songs),
                      onTap: () {
                        context.go("/songs");
                      }),
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      title: Text(t(context).routeNames.artists),
                      onTap: () {
                        context.go("/artists");
                      }),
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      title: Text(t(context).routeNames.genres),
                      onTap: () {
                        context.go("/genres");
                      }),
                if (!(widget.isLoggedIn != null && widget.isLoggedIn == true))
                  ListTile(
                      title: Text(t(context).routeNames.login),
                      onTap: () {
                        context.go("/login");
                      }),
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      title: Text(t(context).routeNames.preferences),
                      onTap: () {
                        context.go("/preferences");
                      }),
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      titleAlignment: ListTileTitleAlignment.bottom,
                      title: Text(t(context).routeNames.management),
                      onTap: () {
                        context.go("/management");
                      }),
                if (widget.isLoggedIn != null && widget.isLoggedIn == true)
                  ListTile(
                      titleAlignment: ListTileTitleAlignment.bottom,
                      title: const Text("Log out"),
                      onTap: () async {
                        await UserPreferencesProvider.logout();
                        // ignore: use_build_context_synchronously
                        context.go("/login");
                      }),
                ListTile(
                    title: const Text("About"),
                    onTap: () {
                      context.go("/about");
                    }),
                const Divider(),
              ])),
              Expanded(
                child: Align(
                  alignment: FractionalOffset.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    child: languageMenu(context, widget.env),
                  ),
                ),
              ),
            ])));
  }
}
