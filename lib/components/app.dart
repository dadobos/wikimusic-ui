import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wikimusic_ui/dsl/dsl.dart';
import 'package:wikimusic_ui/dsl/env.dart';
import 'package:wikimusic_ui/l10n/l10n.dart';

Widget makeCardImage(final BuildContext context, final String contentUrl) =>
    ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Image.network(
          contentUrl,
          alignment: Alignment.center,
          repeat: ImageRepeat.noRepeat,
          fit: BoxFit.cover,
          width: deviceWidth(context, scale: 0.2),
        ));

Widget makeDetailImage(final BuildContext context, final String contentUrl) =>
    ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Image.network(
          contentUrl,
          alignment: Alignment.center,
          repeat: ImageRepeat.noRepeat,
          fit: BoxFit.cover,
          height: deviceHeight(context, scale: 0.4),
        ));

Widget makeImageOrCarousel(
    final BuildContext context, final List<String> artworks) {
  if (artworks.isEmpty) {
    return const Text("No artwork added yet!");
  } else if (artworks.length == 1) {
    return makeDetailImage(context, artworks.first);
  } else {
    return CarouselSlider(
        items: artworks
            .map(
              (final e) => makeDetailImage(context, e),
            )
            .toList(),
        options: CarouselOptions(
          height: (MediaQuery.of(context).size.height * 0.4),
          aspectRatio: 16 / 9,
          viewportFraction: 0.8,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: true,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          onPageChanged: (final index, final reason) {},
          scrollDirection: Axis.horizontal,
        ));
  }
}

Widget makeSubTitleFromOpinions(final BuildContext context, final int likes,
        final int dislikes, final MainAxisAlignment mainAxisAlignment) =>
    Row(mainAxisAlignment: mainAxisAlignment, children: [
      Icon(
        Icons.thumb_up,
        color: Colors.green,
        size: Theme.of(context).textTheme.bodySmall!.fontSize!,
      ),
      Text(
        '$likes',
        style: TextStyle(
            color: Colors.green,
            fontWeight: FontWeight.bold,
            fontSize: Theme.of(context).textTheme.bodySmall!.fontSize!),
      ),
      horizontalSpacer(),
      Icon(
        Icons.thumb_down,
        color: Colors.red,
        size: Theme.of(context).textTheme.bodySmall!.fontSize!,
      ),
      Text(
        '$dislikes',
        style: TextStyle(
            color: Colors.red,
            fontWeight: FontWeight.bold,
            fontSize: Theme.of(context).textTheme.bodySmall!.fontSize!),
      )
    ]);

Widget arrowUpButton(final VoidCallback eff) =>
    ElevatedButton(onPressed: eff, child: const Icon(Icons.arrow_upward));

Widget arrowDownButton(final VoidCallback eff) =>
    ElevatedButton(onPressed: eff, child: const Icon(Icons.arrow_downward));

Widget languageMenu(final BuildContext context, final Env env) {
  final supportedLocales = {"en": "English (US)", "nl": "Nederlands (NL)"};

  return DropdownMenu<String>(
    initialSelection:
        TranslationProvider.of(context).languageSettings.value.locale,
    onSelected: (final String? value) async {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString("locale", value!);
      LanguageChanged(
              languageSettings: LanguageSettings(
                  appLocalization:
                      value == "nl" ? nlAppLocalization : enAppLocalization,
                  locale: value))
          // ignore: use_build_context_synchronously
          .dispatch(context);
    },
    dropdownMenuEntries:
        supportedLocales.entries.map<DropdownMenuEntry<String>>((final e) {
      return DropdownMenuEntry<String>(value: e.key, label: e.value);
    }).toList(),
  );
}

List<Widget> makeButtonsFromOpinions(final int likes, final int dislikes,
        {final VoidCallback? onLike, final VoidCallback? onDislike}) =>
    [
      ElevatedButton(
          onPressed: onLike,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(
                Icons.thumb_up,
                color: Colors.green,
              ),
              Text(
                '  $likes',
                style: const TextStyle(
                    color: Colors.green, fontWeight: FontWeight.bold),
              ),
            ],
          )),
      ElevatedButton(
          onPressed: onDislike,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(
                Icons.thumb_down,
                color: Colors.red,
              ),
              Text(
                '  $dislikes',
                style: const TextStyle(
                    color: Colors.red, fontWeight: FontWeight.bold),
              )
            ],
          ))
    ];

Card makeListCard(
    final BuildContext context,
    final int likes,
    final int dislikes,
    final String? contentUrl,
    final String identifier,
    final String displayName,
    final int viewCount,
    final VoidCallback onTap,
    {final String? meta}) {
  final img = switch (contentUrl) {
    null => const Icon(Icons.music_note),
    (final String u) => makeCardImage(context, u)
  };

  return Card(
    clipBehavior: Clip.hardEdge,
    child: InkWell(
      splashColor: Colors.blue.withAlpha(30),
      onTap: onTap,
      mouseCursor: WidgetStateMouseCursor.clickable,
      child: ListTile(
        leading: img,
        trailing: PopupMenuButton(
          itemBuilder: (final context) => [
            PopupMenuItem<Widget>(
              child: const Text("Copy identifier"),
              onTap: () {
                Clipboard.setData(ClipboardData(text: identifier));
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                      content: Text(
                          'Copied $displayName identifier to clipboard - $identifier')),
                );
              },
            )
          ],
        ),
        title: wrap(children: [
          Text(
            displayName,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          if (meta != null && meta != "") Text(meta),
        ]),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            wrap(children: [
              makeSubTitleFromOpinions(
                  context, likes, dislikes, MainAxisAlignment.start),
              Text("$viewCount ${t(context).views}")
            ])
          ],
        ),
        isThreeLine: true,
      ),
    ),
  );
}
