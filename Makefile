fmt:
	dart run build_runner build
	find . -name '*.nix' -exec nixfmt {} \;
	dart fix --apply
	dart format lib
	-statix check
	-deadnix -f

# dev-web: fmt	
#	dart run build_runner build && watchexec -r -e dart,yaml,json,html "flutter build --verbose web --web-renderer canvaskit"
serve-web:
	simple-http-server -i -p 5173 ./build/web	
dev-web: fmt
	flutter run -d chrome --web-port=5173 --web-hostname=127.0.0.1 --dart-define=API_URL=http://127.0.0.1:50050

dev-android: fmt
	dart run build_runner build
	flutter build --verbose apk
android-emu:	
	flutter emulators --launch flutter_emulator


dev-linux: fmt
	dart run build_runner build && watchexec -r -e dart,yaml,json "flutter build --verbose linux && ./build/linux/x64/release/bundle/wikimusic_ui --dart-define=API_URL=http://127.0.0.1:50050"	

aws-upload-beta:
	tar -czf master-release.tar.gz README.org COPYING build/web/*
	aws s3 ls
	aws s3 ls s3://beta.wikimusic.jointhefreeworld.org/
	aws s3 rm s3://beta.wikimusic.jointhefreeworld.org --recursive
	aws s3 cp build/web s3://beta.wikimusic.jointhefreeworld.org/ --recursive
	aws s3 ls s3://beta.wikimusic.jointhefreeworld.org/

aws-upload:
	tar -czf master-release.tar.gz README.org COPYING build/web/*
	aws s3 ls
	aws s3 ls s3://wikimusic.jointhefreeworld.org/
	aws s3 rm s3://wikimusic.jointhefreeworld.org --recursive
	aws s3 cp build/web s3://wikimusic.jointhefreeworld.org/ --recursive
	aws s3 ls s3://wikimusic.jointhefreeworld.org/

release-web: fmt	
	dart run build_runner build
	flutter build web --web-renderer canvaskit --release
release-linux: fmt	
	flutter build linux --release
release-apk: fmt	
	flutter build apk --release	
run-android:
	flutter run --release	
